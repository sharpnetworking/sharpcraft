﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Cbg
{
    [Serializable]
    public struct Point
    {
        public int x, y;

        public Point(int x_, int y_) { x = x_; y = y_; }
        public override string ToString()
        {
            return "[" + x + ", " + y + "]";
        }

        public static Point operator +(Point p1, Point p2) { return new Point(p1.x + p2.x, p1.y + p2.y); }
        public static Point operator -(Point p1) { return new Point(-p1.x, -p1.y); }

        public static Point operator -(Point p1, Point p2) { return p1 + -p2; }

        public override bool Equals(object comparand) { return this.ToString().Equals(comparand.ToString()); }
        public override int GetHashCode() { return this.ToString().GetHashCode(); }

        public static bool operator ==(Point o1, Point o2) { return System.Object.Equals(o1, o2); }
        public static bool operator !=(Point o1, Point o2) { return !(o1 == o2); }

        public static IEnumerable<Point> Range(Point size)
        {
            Point p = new Point();
            for (p.y = 0; p.y < size.y; ++p.y)
                for (p.x = 0; p.x < size.x; ++p.x)
                    yield return p;
        }
        public static IEnumerable<Point> SymmetricRange(Point size)
        {
            Point p = new Point();
            for (p.y = -size.y; p.y <= size.y; ++p.y)
                for (p.x = -size.x; p.x <= size.x; ++p.x)
                    yield return p;
        }

        public static Point Zero { get { return new Point(0, 0); } }
        public static Point One { get { return new Point(1, 1); } }

        public static Point Scale(Point p, Point scale) { return new Point(p.x * scale.x, p.y * scale.y); }

        public static Point FromString(string s)
        {
            List<string> ls = new List<string>();

            string sp = "";
            foreach (char c in s.ToCharArray())
            {
                if (c == ' ' || c == '[' || c == ']' || c ==',')
                {
                    if (sp != "")
                        ls.Add(sp);
                    sp = "";
                }
                else
                    sp += c;
            }

            return new Point(Convert.ToInt32(ls[0]), Convert.ToInt32(ls[1]));
        }
    }
    
    [Serializable]
    public class Plane<T>
    {
        public T[] plane;
        public Point Size { get; set; }

        public Plane() { }
        public Plane(Point size) { plane = new T[size.x * size.y]; Size = size; }

        void AssertRange(int x, int y)
        {
            if (!InRange(x, y))
                throw new IndexOutOfRangeException(new StringBuilder().AppendFormat("Plane<{0}>{1} w/ size {2}", typeof(T).Name, new Point(x, y), Size).ToString());
        }

        public bool InRange(Point p)
        {
            return InRange(p.x, p.y);
        }

        public bool InRange(int x, int y)
        {
            if (x < 0 || x >= Size.x)
                return false;
            if (y < 0 || y >= Size.y)
                return false;
            return true;
        }

        public T this[Point pos]
        {
            get { return this[pos.x, pos.y]; }
            set { this[pos.x, pos.y] = value; }
        }
        public T this[int x, int y]
        {
            get { AssertRange(x, y); return plane[y * Size.x + x]; }
            set { AssertRange(x, y); plane[y * Size.x + x] = value; }
        }

        public IEnumerable<T> GetTiles()
        {
            foreach (Point p in Point.Range(Size))
                yield return this[p];
        }
        public IEnumerable<KeyValuePair<Point, T>> GetEnum()
        {
            foreach (Point p in Point.Range(Size))
                yield return new KeyValuePair<Point, T>(p, this[p]);
        }
    }

    public class PermutationFinder<T>
    {
        private T[] items;
        private Predicate<T[]> SuccessFunc;
        //private bool success = false;
        private int itemsCount;
        
        public bool Evaluate(T[] items, Predicate<T[]> SuccessFunc)
        {
            this.items = items;
            this.SuccessFunc = SuccessFunc;
            this.itemsCount = items.Count();
            
            return Recurse(0);
        }
        
        private bool Recurse(int index)
        {
            T tmp;
            
            if (index == itemsCount)
            {
                return SuccessFunc(items);
            }
            else
            {
                for (int i = index; i < itemsCount; i++)
                {
                    tmp = items[index];
                    items[index] = items[i];
                    items[i] = tmp;
                    
                    if(Recurse(index + 1))
                        return true;
                    
                    tmp = items[index];
                    items[index] = items[i];
                    items[i] = tmp;
                }
                return false;
            }
        }
    }

    public static class Tools
	{
        static public void Assert(bool b)
        {
            Debug.Assert(b);

            if (!b)
                throw new Exception("assert failed");
        }

        static System.Random chanceRand = new Random();
        
        static public bool Chance(double aChance)
        {
            return chanceRand.NextDouble() <= aChance;
        }
        
        static public double RandomValue{ get { return chanceRand.NextDouble(); } }

        static public double Range (double max) { return RandomValue * max; }

        static public int WeightedRandom(IEnumerable<double> weights)
        {
            double random = chanceRand.NextDouble();
            double totalWeight = weights.Sum();

            Tools.Assert(totalWeight > 0);

            random *= totalWeight;

            int counter = 0;
            double cumulative = 0;

            foreach (double w in weights)
            {
                cumulative += w;

                if (random < cumulative)
                    return counter;

                ++counter;
            }

            return 0;
        }

        public static string CombineWords(IEnumerable<string> words)
        {
            if (words == null || words.Count() == 0)
                return "";
            if (words.Count() == 1)
                return words.First();
            string r = "";
            foreach (var e in words.Take(words.Count()-1))
                r += e + ", ";
            r += " and " + words.Last();
            return r;
        }


        public static double Clamp(double aVal, double aMin, double aMax)
        {
            return System.Math.Min(aMax, System.Math.Max(aMin, aVal));
        }

        public static double Clamp01(double aVal)
        {
            return Clamp(aVal, 0, 1);
        }

        public static int Poisson(double mean)
        {
            double L = Math.Exp(-mean);
            int k = 0;
            double p = 1;
            do
            {
                k++;
                p *= chanceRand.NextDouble();
            }
            while (p > L);

            return k - 1;
        }
        public static double UniformDistribution(double mean)
        {
            return Range(mean * 2);
        }
        
        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = chanceRand;
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static double RunAverage(Func<double> f, int repeats)
        {
            double sum = 0;

            for (int i = 0; i < repeats; ++i)
                sum += f.Invoke();

            return sum / repeats;
        }

        public static int Mod(int x, int m)
        {
            return (x % m + m) % m;
        }
    }

    public class RunningAverage
    {
        private double sum = 0;
        private int count = 0;

        public double Get { get { return sum / count; } }
        public double GetOrDefault
        { 
            get
            { 
                if(count == 0)
                    return 0;
                else
                    return sum / count; 
            } 
        }
        public void Add(double d) { sum += d; count++; }
    }
}
