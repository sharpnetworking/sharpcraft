﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;
namespace Cbg
{
    public class Formulas
    {

        public static float itemDecayChance = 0.01f;
        public static int daysPerDude = 5;
        public static double DeathChance(Dude d)
        {
            double chance = (1 - d.HP) / (d.HealthScore) / 2;
            chance -= d.GetSkill("survival")/100d;

            return Tools.Clamp01(chance);
        }


    }
}