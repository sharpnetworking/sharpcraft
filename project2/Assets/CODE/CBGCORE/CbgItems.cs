﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace Cbg
{

    
    public class Item{
        public string[] Tags{ get; private set; }
        public string Name{get{return Tags[0];}}
        public bool HasTag(string aTag){
            return Tags.Contains(aTag);
        }
        
        public Item(params string[] aTags)
        {
            Tags = aTags.ToArray();
        }
        
        public static Item Create(string aName)
        {
            var items = Constants.items.FirstOrDefault(e => e [0] == aName);
            if(items != null)
                return new Item(items);
            else
                return new Item(new string[]{aName}); //ha
            
        }
    }
    
    public class ItemStack
    {
        public Item item;
        public int count;
        
        public ItemStack(){}
        public ItemStack(Item aItem, int aCount){
            item = aItem;
            count = aCount;
        }
        
        public override string ToString()
        {
            return count == 1 ? item.Name : item.Name + " x" + count;
        }
    }

    public class ItemSet
    {
        public ItemStack[] items;
        public ItemSet(string aItems)
        {
            string[] split = aItems.Split(',');
            items = split.Select(e => e.Trim()).Select(e=>new ItemStack(new Item(ParseItemName(e)),ParseItemCount(e))).ToArray();
        }

        public ItemSet(Item aItem)
        {
            items = new ItemStack[1]{new ItemStack(aItem,1)};
        }
        public static int ParseItemCount(string aItem)
        {
            //Debug.Log(new string(aItem.Reverse().TakeWhile(e => e != '_').Reverse().ToArray()));
            if (aItem.Contains("_"))
                return int.Parse(new string(aItem.Reverse().TakeWhile(e=>e!='_').Reverse().ToArray()));
            return 1;
        }
        public static string ParseItemName(string aItem)
        {
            return new string(aItem.TakeWhile(e=>e!='_').ToArray());
        }

        public override string ToString()
        {
            return string.Join(", ", (from st in items select st.ToString()).ToArray());
        }
    }

    public class TagGroupSet
    {
        public TagGroup[] tagGroups;
        public TagGroupSet(string aTags)
        {
            string[] split = aTags.Split(',');
            tagGroups = split.Select(
                delegate(string tagGroupString)
            {
                int count = ItemSet.ParseItemCount(tagGroupString);
                tagGroupString = ItemSet.ParseItemName(tagGroupString);
                return new TagGroup(tagGroupString.Trim().Split('+').Select(f => f.Trim()).ToArray(), count);
            }
            ).ToArray();
        }
    
        public override string ToString()
        {
            return string.Join(", ", (from st in tagGroups select st.ToString()).ToArray());
        }
    }
    public class TagGroup
    {
        public string[] tags;
        public int count;
        public TagGroup(string[] aTags, int aCount)
        {
            tags = aTags;
            count = aCount;
        }

        public override string ToString()
        {
            return string.Join("+", tags) + " x" + count;
        }
    }

    public class Recipe
    {
        public ItemSet output;
        public TagGroupSet input;

        public static Recipe Create(string aOutput, string aInput)
        {
            Recipe r = new Recipe();
            r.output = new ItemSet(aOutput);
            r.input = new TagGroupSet(aInput);
            return r;
        }

    }
}
