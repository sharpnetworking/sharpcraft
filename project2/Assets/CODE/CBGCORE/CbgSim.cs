﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cbg
{
    public class FightResult
    {
        public RunningAverage totalDamage = new RunningAverage();
        public RunningAverage nonlethalDamage = new RunningAverage();
        public RunningAverage deathRatio = new RunningAverage();
    }

    class SimulationCache
    {
        private int worldState = 0;
        private Dictionary<string, FightResult> simulations = new Dictionary<string, FightResult>();

        public FightResult Get(int worldState, Point p, Dude d, bool hunt)
        {
            if (this.worldState != worldState)  // purge on world state change
            {
                this.worldState = worldState;
                simulations.Clear();
            }

            string id = p + " " + d.ID + " " + hunt;

            if (!simulations.ContainsKey(id))
                simulations.Add(id, new FightResult());

            return simulations[id];
        }

    }
    
    public class FightSimulator
	{
        private SimulationCache cache = new SimulationCache();

        public FightResult Get(int worldState, Point location, Dude selectedDude, bool hunt)
        {
            FightResult fr = cache.Get(worldState, location, selectedDude, hunt);

            var a = new Cbg.ActionHunt(selectedDude, location);

            SimulateFight(a, hunt, ref fr);

            return fr;
        }
        
        static private void SimulateFight(Cbg.ActionHunt a, bool hunt,
            ref FightResult fr)
        {
            int count = 10;

            for (int i = 0; i < count; ++i)
            {
                double damage;
                bool dead = a.Fight(false, hunt, CbgGameManager.Inst.Data, null, out damage);

                fr.totalDamage.Add(damage);

                if (dead)
                    fr.deathRatio.Add(1);
                else
                {
                    fr.deathRatio.Add(0);
                    fr.nonlethalDamage.Add(damage);
                }
            }
        }

	}
}
