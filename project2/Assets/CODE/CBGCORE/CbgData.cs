using System.Collections.Generic;
using System;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Cbg
{
    public class GameData {
        //other worlddata
        public int GameTime{ get; set; }
    	//dudes
        public List<Dude> Dudes { get; private set; }
        public Dude GetDude(Guid aId)
        {
            return Dudes.FirstOrDefault(e => e.ID == aId);
        }

    	//map
        public WorldMap Map { get; set; }

    	//items
        public Inventory Inventory { get; private set; }

        int fireConsume = 1;
        public int FireConsume { get { return fireConsume; } set { fireConsume = value; FireAdjust(); } }
        void FireAdjust()
        {
            fireConsume = Tools.Mod(fireConsume, 6);
        }

        public int WorldState { get; private set; }
        public void ChangeWorld() { WorldState++; }

        public GameData()
        {
            GameTime = 0;
            Inventory = new Inventory();
            Map = null;
            Dudes = new List<Dude>();

        }

        public static bool godMode = false;
    }

    public class Inventory{

        Dictionary<string, ItemStack> items = new Dictionary<string,ItemStack>();

        public IEnumerable<ItemStack> Items{ get { return items.Values; } }

        public void Remove(Item m)
        {
            if (items.ContainsKey(m.Name))
            {
                items[m.Name].count -= 1;
                if(items[m.Name].count == 0)
                    items.Remove(m.Name);
            }
        }
        public void Add(Item m, int count = 1)
        {
            if (!items.ContainsKey(m.Name))
            {
                ItemStack st = new ItemStack() { item = m, count = 0 };
                items.Add(m.Name, st);
            }

            items[m.Name].count += count;
        }
        public int ItemCount(string aItem)
        {
            if (items.ContainsKey(aItem))
                return items [aItem].count;
            return 0;
        }
        public bool HasItem(string aItem)
        {
            return ItemCount(aItem) > 0;
        }

        public ItemStack GetItemStack(string aName)
        {
            ItemStack r = null;
            items.TryGetValue(aName, out r);
            return r;
        }

        public Item GetItem(string aName)
        {
            ItemStack r = GetItemStack(aName);
            if (r != null && r.count > 0)
                return r.item;
            return null;
        }

        public Item TryGetItemByTags(params string[] tags)
        {
            var arr = from st in items.Values
                    select st.item;

            foreach(Item i in arr)
            {
                bool valid = true;
                foreach (string tag in tags)
                    if (!i.HasTag(tag))
                    {
                        valid = false;
                        break;
                    }
                if (valid)
                    return i;
            }
            return null;
        }

        public void ConsumeItemByTags(params string[] tags)
        {
            Item i = TryGetItemByTags(tags);
            Tools.Assert(i != null);
            Remove(i);
        }
    }

    public enum ExpAdd {DEFAULT, NEW_LEVEL, MAX};

    public class DudeSkillTree
    {
        public const int maxExp = 250;
        public const int levelBranch = 3;

        Dictionary<string, int> skillTree = new Dictionary<string, int>();

        public DudeSkillTree()
        {
            foreach (var e in Constants.skills)
                skillTree [e] = 0;
        }

        public ExpAdd AddExp(string skill, double xp, double adjust)
        {
            xp *= (1 + adjust);

            UnityEngine.Debug.Log(skill + " " + xp);

            int xpExact = Tools.Poisson(xp);

            return AddExpExact(skill, xpExact);
        }

        public ExpAdd AddExpExact(string skill, int xp)
        {
            int lvl_before = GetLevel(skill);

            if (!skillTree.ContainsKey(skill))
                skillTree[skill] = 0;

            if (skillTree[skill] == maxExp)
                return ExpAdd.MAX;

            skillTree[skill] += xp;

            if (skillTree[skill] > maxExp)
                skillTree[skill] = maxExp;

            int lvl_after = GetLevel(skill);

            if (lvl_after != lvl_before)
                return ExpAdd.NEW_LEVEL;
            else
                return ExpAdd.DEFAULT;
        }

        public int GetLevel(string skill)
        {
            if (!skillTree.ContainsKey(skill))
                return 0;

            for (int i = 1; i <= 5; ++i)
            {
                if (skillTree[skill] < i * i * 10)
                    return i - 1;
            }

            return 5;
        }
        public int GetExp(string skill)
        {
            if (!skillTree.ContainsKey(skill))
                return 0;
            else
                return skillTree[skill];
        }
        public IEnumerable<KeyValuePair<string, int>> GetAllSkills()
        {
            foreach (var kv in skillTree)
            {
                int lvl = GetLevel(kv.Key);
                if(kv.Value != 0)
                    yield return new KeyValuePair<string, int>(kv.Key, lvl);
            }
        }

    }

    public enum Stat { Attack, Defense }
    
    public class DudeStats
    {
        Dictionary<Stat, double> statValues = new Dictionary<Stat, double>();

        public DudeStats()
        {
            foreach (Stat s in (Stat[])Enum.GetValues(typeof(Stat)))
                statValues.Add(s, 0);
        }

        public DudeStats(Stat s, double val)
            :this()
        {
            AddStat(s, val);
        }

        public double GetStat(Stat s) { return statValues[s]; }
        public void AddStat(Stat s, double val) { statValues[s] += val; }

        public static DudeStats Attack(double val) { return new DudeStats(Stat.Attack, val); }
        public static DudeStats Defense(double val) { return new DudeStats(Stat.Defense, val); }

        static public DudeStats operator +(DudeStats ds1, DudeStats ds2)
        {
            DudeStats ret = new DudeStats();

            foreach (Stat s in ret.statValues.Keys.ToArray())
                ret.statValues[s] = ds1.statValues[s] + ds2.statValues[s];

            return ret;
        }
        static public DudeStats operator *(DudeStats ds1, double d)
        {
            DudeStats ret = new DudeStats();

            foreach (Stat s in ret.statValues.Keys.ToArray())
                ret.statValues[s] = ds1.statValues[s] * d;

            return ret;
        }
    }

    public class FightStats
    {
        public double attack = 0;
        public double defense = 0;
        public double hp = 0;

        public FightStats(){}
        public FightStats(double attack_, double defense_, double hp_) { attack = attack_; defense = defense_; hp = hp_; }
        public FightStats(FightStats fs) : this(fs.attack, fs.defense, fs.hp) { }

        static public double DamageRoll(double attack, double defence)
        {
            double AttackRoll = Tools.Range(attack);
            double DefenseRoll = Tools.Range(defence);

            if (AttackRoll > DefenseRoll)
                return AttackRoll - DefenseRoll;
            else
                return 0;
        }
        static public void Attack(FightStats fs1, FightStats fs2)
        {
            fs2.hp -= DamageRoll(fs1.attack, fs2.defense);
        }
        static public void Fight(FightStats fs1, FightStats fs2)
        {
            for (int i = 0; i < 100; ++i)
            {
                if (fs1.hp <= 0 || fs2.hp <= 0)
                    return;
                
                Attack(fs1, fs2);
                Attack(fs2, fs1);
            }
        }

        static public double AverageFightLength(double attack, double defense, double defenceHealth)
        {
            double averageDamage = FightDataCache.Get().GetDamage((int)attack, (int)defense).Value;

            return defenceHealth / averageDamage;
        }
        static public double AverageHealthLoss(FightStats fs1, FightStats fs2)
        {
            double length = AverageFightLength(fs1.attack, fs2.defense, fs2.hp);
            double averageDamageRecieved = FightDataCache.Get().GetDamage((int)fs2.attack, (int)fs1.defense).Value;

            return averageDamageRecieved * length;
        }
    }

    [Serializable]
    public class FightData
    {
        int maxAttack;
        int maxDefence;

        public FightData(int maxAttack_, int maxDefence_) { maxAttack = maxAttack_; maxDefence = maxDefence_; }

        public void RunAverages(Func<int, int, double> f, int repeats)
        {
            for (int i = 0; i <= maxAttack; ++i)
                for (int j = 0; j <= maxDefence; ++j)
                    damages.Add(i + " " + j, Tools.RunAverage(() => f(i,j), repeats));
        }

        Dictionary<string, double> damages = new Dictionary<string,double>();

        public double? GetDamage(int attack, int defence)
        {
            string key = attack + " " + defence;
            if (!damages.ContainsKey(key))
                return null;
            else
                return damages[key];
        }
    }

    public static class FightDataCache
    {
        static FightData fd = null;
        const string filename = "FightData2.binary";

        public static FightData Get()
        {
            if (fd != null)
                return fd;

            if (File.Exists(filename))
            {
                FileStream stream = File.Open(filename, FileMode.Open);
                var formatter = new BinaryFormatter();
                fd = (FightData)formatter.Deserialize(stream);
                stream.Close();
            }
            else
            {
                fd = new FightData(30, 20);
                fd.RunAverages((i,j) => FightStats.DamageRoll(i,j), 1000);

                FileStream stream = File.Create(filename);
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, fd);
                stream.Close();
            }

            return fd;
        }
    }

    public class Dude
    {
        static SCG.General.MarkovNameGenerator sGenerator = null;

        public Guid ID {get;private set;}
        public string Name { get; private set; }
        public bool ActionTakenToday{get; set;}

        double mHP = 1; 
        double mWarmth = .2;
        double mSatiation = .2;
        public double HP { get { return mHP; } set { mHP = Tools.Clamp01(value); } }
        public double Warmth { get { return mWarmth; } set { mWarmth = Tools.Clamp01(value); } }
        public double Satiation { get { return mSatiation; } set { mSatiation = Tools.Clamp01(value); } }

        public double WarmthScore { get { return Warmth / .2; } }
        public double SatiationScore { get { return Satiation / .2; } }
        public double HealthScore { get { return (WarmthScore + SatiationScore)/2; } }

        public double ExpAdjust { get { return (HealthScore - 3) * .25; } }
        public double RestHP { get { return 4 + HealthScore + skills.GetLevel("survival"); } }

        int foodPerDay = 2;
        public int FoodPerDay { get { return foodPerDay; } set { foodPerDay = value; FoodAdjust(); } }
        void FoodAdjust()
        {
            foodPerDay -= 2;
            foodPerDay = Tools.Mod(foodPerDay, 5);
            foodPerDay += 2;
        }

        public void FoodAdvance(int food)
        {
            double HUNGER = .21;
            
            // lazy integration
            for(int i = 0; i < food; ++i)
            {
                double level = SatiationScore;
                if (level < .7)  // clamp; can't survive on 1 food
                    level = .7;
                double satiationChange = HUNGER / (level + 1);    // satiation rate formula
                mSatiation += satiationChange;
                mSatiation -= HUNGER / food;  // hunger
            }

            if (food == 0)
                mSatiation -= HUNGER;
            
            mSatiation = Tools.Clamp01(mSatiation);
        }
        public void HeatAdvance(double heat)
        {
            double COLD = .21;

            int div = 2;
            
            // lazy integration
            for (int i = 0; i < div; ++i)
            {
                double level = WarmthScore;
                if (level < .8)  // clamp
                    level = .8;

                double heatAdd = COLD / level * heat/div;
                double heatLoss = COLD / div;

                //Warmth += (heat - level) * COLD / div;
                Warmth += (heatAdd - heatLoss);
            }
        }

        public DudeSkillTree skills = new DudeSkillTree();
        public int GetSkill(string skill) { return skills.GetLevel(skill); }

        public DudeStats stats = new DudeStats(Stat.Attack, 1);

        public Dude()
        {
            if(sGenerator == null)
                sGenerator = new SCG.General.MarkovNameGenerator((new String(Cbg.Constants.nameList.Where(c => !char.IsPunctuation(c)).ToArray())).Split(' '),3,5);

            Name = sGenerator.NextName;
            ID = Guid.NewGuid();
        }

        private Dude(int n){}
        static public Dude GetTestDude() { return new Dude(0); }

        //stat information
        //resource allocation information
        //owned resource information???
        //skill info
    }
    
    public class ItemYield
    {
        public string item;
        public double yield;

        public ItemYield(string item_, double yeild_)
        {
            item = item_;
            yield = yeild_;
        }
    }
    
    public class ActivityYield
    {
        public ItemYield[] items;
        public int hazards;

        public ActivityYield(ItemYield[] items_, int hazards_)
        {
            items = items_;
            hazards = hazards_;
        }
    }

    public class MonsterYield
    {
        public Dictionary<string, double> items = new Dictionary<string, double>();

        public MonsterYield() { }
        public MonsterYield(string item, double yield)
        {
            items.Add(item, yield);
        }

        public void Add(MonsterYield my)
        {
            foreach(string itemName in my.items.Keys)
            {
                if(items.ContainsKey(itemName))
                    items[itemName] += my.items[itemName];
                else
                    items[itemName] = my.items[itemName];
            }
        }

        static public MonsterYield operator +(MonsterYield my1, MonsterYield my2)
        {
            MonsterYield ret = new MonsterYield();

            ret.Add(my1);
            ret.Add(my2);

            return ret;
        }

        static public MonsterYield operator *(MonsterYield my, double d)
        {
            MonsterYield ret = new MonsterYield();

            foreach (string itemName in my.items.Keys)
                ret.items[itemName] = my.items[itemName] * d;

            return ret;
        }
        
        static public MonsterYield Basic(double size)
        {
            MonsterYield ret = new MonsterYield();
            
            //ret += new MonsterYield("meat", 2);
            //ret += new MonsterYield("bone", 3);
            ret += new MonsterYield("skin", 1);

            ret *= size;

            return ret;
        }
    }

    public class Monster
    {
        public string name;
        public FightStats stats;
        public MonsterYield yield;

        public Monster(string name_, double attack_, double defense_, double hp_, MonsterYield yield_)
        {
            name = name_;
            stats = new FightStats(attack_, defense_, hp_);
            yield = yield_;
        }
    }

    public class Tile
    {
        public string Name { get; private set; }
        public ActivityYield gahtering;
        public ActivityYield hunting;

        public Tile(string name_, ActivityYield gahtering_, ActivityYield hunting_)
        {
            Name = name_;
            gahtering = gahtering_;
            hunting = hunting_;
        }
    }

    public class WorldMapTile
    {
        public Point location;
        public Tile tile;
        //location information (index)
    	//resource information
    	//ecology information
    }

    public class WorldMap
    {
        private WorldMap() { }
        
        //TODO should probably not be public..
    	public Plane<WorldMapTile> mMap;
        //TODO turn into accessors
        public Point size;
        public static WorldMap Generate(Point size)
    	{
            WorldMap wm = new WorldMap();
            wm.size = size;
            wm.mMap = new Plane<WorldMapTile>(size);

            List<Tile> tiles = new List<Tile>(Constants.tiles);
            List<double> weights = new List<double>();

            int snowIndex = -1;
            int desertIndex = -1;

            foreach (var s in tiles)
            {
                if (s.Name == "snow")
                    snowIndex = weights.Count;
                if (s.Name == "desert")
                    desertIndex = weights.Count;

                weights.Add(1.0);
            }

            Tools.Assert(snowIndex != -1);
            Tools.Assert(desertIndex != -1);

    		foreach(Point p in Point.Range(size))
            {
                // snow generation
                double fSnowMult = 1 - (double)p.y / size.y * 2;
                if (fSnowMult > 0)
                    weights[snowIndex] = 10 * fSnowMult;
                else
                    weights[snowIndex] = 0;

                // desert generation
                double fDsrtMult = 1 - (double)(size.y - p.y) / size.y * 2;
                if (fDsrtMult > 0)
                    weights[desertIndex] = 10 * fDsrtMult;
                else
                    weights[desertIndex] = 0;
                
                Tile tile = tiles[Tools.WeightedRandom(weights)];
                
                //TODO random generation, perlin noise???
                wm.mMap[p] = new WorldMapTile() { location = p, tile = tile };
            }

            return wm;
    	}
    }
}