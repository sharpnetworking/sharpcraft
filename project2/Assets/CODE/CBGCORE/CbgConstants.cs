﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
namespace Cbg
{
    public static class Constants{
        //gui positions
        public static int guiPadding = 15;
        public static int mapWidth = 300;
        public static int topSectionHeight = 300;
        public static int messageBoxWidth = 400;
        public static int windowWidth = guiPadding * 1 + mapWidth + messageBoxWidth;
        public static int bottomSectionHeight = 500;
        public static int windowHeight = guiPadding * 1 + bottomSectionHeight + topSectionHeight;
        public static int dudeBoxWidth = 400;
        public static int inventoryBoxWidth = windowWidth - guiPadding * 1 - dudeBoxWidth; 
        public static int actionBoxWidth = inventoryBoxWidth;
        public static int inventoryBoxHeight = 200;
        public static int actionBoxHeight = windowHeight - guiPadding*2 - inventoryBoxHeight; 





        public static string nameList = "Quisque et fermentum lacus. In scelerisque consectetur lorem, ut ornare sapien. Nunc eu ex interdum lacus volutpat vulputate ac quis augue. Quisque enim velit, tincidunt nec viverra id, ultricies a ipsum. Nam quis ante lorem. Nullam vel purus eros. Nam aliquam aliquet pharetra.";
        public static string[] skills = new string[]
        {
            "hunting","gathering","survival","crafting"
        };

       

        public static string[] actions = new string[]
        {
            "gather","hunt","craft","cook","rest","fire","advance"
        };

        //TODO same item with diff tag?
        //maybe tags are optional
        public static string[][] items = new string[][]
        {
            new string[]{"stick","burnable", "choppable"},
            new string[]{"branch","burnable", "choppable"},
            new string[]{"firewood"},

            new string[]{"rock"},
            
            new string[]{"berry","edible"},
            new string[]{"fruit","edible"},

            new string[]{"snow"},
            new string[]{"sand"},

            new string[]{"fireplace","permanent"},

            //new string[]{"wood","big","stick","burnable","carvable", "choppable"},
            //new string[]{"root","edible"},
            //new string[]{"weed","burnable"},
            //new string[]{"clay"},
            //new string[]{"flower"},
            //new string[]{"mushroom","edible"},
            //new string[]{"giant mushroom","choppable"},
            //new string[]{"nut","edible"},
            //new string[]{"meat","edible"},
            //new string[]{"seeds","edible","burnable","growable"},
            //new string[]{"poop","burnable"},
            //new string[]{"workbench","big","permanent"},
            //new string[]{"basket","burnable"},
            //new string[]{"rope","burnable","fiber"},
            //new string[]{"craftbench","big","permanent"},
            //new string[]{"alchemy bench","big","permanent"},
            //new string[]{"fabric","burnable","sheet"},
            //new string[]{"web","burnable","fiber"},
        };

        public static string[] itemTags = items.SelectMany(e=>e).ToArray();


        public static List<Cbg.Recipe> newRecipes = new List<Cbg.Recipe>()
        {
            Cbg.Recipe.Create("firewood","burnable_10"),
            Cbg.Recipe.Create("firewood_2","burnable_20"),
            Cbg.Recipe.Create("firewood_3","burnable_30"),
            Cbg.Recipe.Create("firewood_4","burnable_40"),
            Cbg.Recipe.Create("firewood_5","burnable_50"),
            
            Cbg.Recipe.Create("skin bag","skin_3"),
            Cbg.Recipe.Create("chopping rock","rock_3"),
            
            Cbg.Recipe.Create("fireplace","rock_3,firewood_3"),
            
            Cbg.Recipe.Create("TEST ITEM DO NOT CRAFT","burnable+carvable_2,rock_3,firewood"),
        };

        public static Dictionary<string,string[][]> recipes = new Dictionary<string, string[][]>()
        {
            {"butcher",     Recipe("butcherable")},
            
            {"firewood",  Recipe(M("stick", 10))},
            {"firewood_2",  Recipe(M("stick", 20))},
            {"firewood_3",  Recipe(M("stick", 30))},
            {"firewood_4",  Recipe(M("stick", 40))},
            {"firewood_5",  Recipe(M("stick", 50))},
            
            {"skin bag",  Recipe(M("skin", 3))},
            {"chopping rock",  Recipe(M("rock", 3))},
            
            {"fireplace",   Recipe(M("rock", 3), M("firewood", 3))},

            //{"mushroom_10", Recipe("giant mushroom")},
            //{"firewood_3",  Recipe(M("burnable", 5))},
            //{"wood",        Recipe(M("stick", 2))},
            //{"fence",       Recipe("stick", "big")},
            //{"basket",      Recipe("stick", "fiber")},

            
            //{"workbench",   Recipe("stick", "fiber", "rock")},
            //{"spear",       Recipe(I("stick", "carvable"),"workbench")},
            
            //{"craftbench",  Recipe("bone", "stick", "rock", "workbench")},
            //{"rope",        Recipe("fiber", "craftbench")},
            //{"axe",         Recipe("stick", "carvable", "rope", "craftbench")},
            //{"fabric",      Recipe("fiber", "craftbench")},
            //{"blanket",     Recipe(M("sheet", 2), "craftbench")},

            //{"alchemy bench",Recipe(I("stick", "big"), "leather", "rock", "poison sack")},
            //{"clothes",     Recipe(M("sheet", 2), "alchemy bench")},
            //{"tent",        Recipe("stick", "sheet", "alchemy bench")},
            //{"house",       Recipe("rock", I("stick", "big"), "leather", "alchemy bench")},
        };

        public static string[] I(params string[] tags) { return tags; }
        public static string[][] Recipe (params object[] items)
        {
            List<string[]> ret = new List<string[]>();

            foreach (object item in items)
            {
                string s = item as string;
                string[] arr = item as string[];
                KeyValuePair<string[], int>? mult = item as KeyValuePair<string[], int>?;

                if (s != null)
                    ret.Add(new string[] { s });
                else if (arr != null)
                    ret.Add(arr);
                else if (mult != null)
                {
                    for (int i = 0; i < mult.Value.Value; ++i )
                        ret.Add(mult.Value.Key);
                }
                else
                    throw new Exception("unexpected type in recipe");
            }

            return ret.ToArray();
        }
        public static KeyValuePair<string[], int> M(string tag, int num)
        {
            return new KeyValuePair<string[], int>(new string[] { tag }, num);
        }
        public static KeyValuePair<string[], int> M(string[] tags, int num)
        {
            return new KeyValuePair<string[], int>(tags, num); 
        }

        public static int ParseItemCount(string aItem)
        {
            if (aItem.Contains("_"))
                return int.Parse(new string(aItem.Reverse().TakeWhile(e=>e!='_').Reverse().ToArray()));
            return 1;
        }
        public static string ParseItemName(string aItem)
        {
            return new string(aItem.TakeWhile(e=>e!='_').ToArray());
        }

        // giant monsters (add modifier?)
        public static Monster[] monsters = new Monster[]
        {
            new Monster("rabbit", 1, 2, 3, MonsterYield.Basic(1)),
            new Monster("rat", 1, 1, 3, MonsterYield.Basic(1)),
            new Monster("goat", 2, 1, 5, MonsterYield.Basic(2)),
            new Monster("sheep", 2, 0, 7, MonsterYield.Basic(2)),
            new Monster("duck", 2, 2, 3, MonsterYield.Basic(1)),
            new Monster("turtle", 1, 3, 1, MonsterYield.Basic(1) + new MonsterYield("shell", 1)),
            new Monster("penguin", 1, 0, 3, MonsterYield.Basic(1)),

            new Monster("spider", 3, 2, 5, MonsterYield.Basic(1) + new MonsterYield("web", 4)),
            new Monster("snake", 4, 4, 3, MonsterYield.Basic(1) + new MonsterYield("poison sac", 1)),
            new Monster("scorpion", 7, 2, 3, MonsterYield.Basic(1) + new MonsterYield("poison sac", 2)),
            new Monster("boar", 3, 3, 7, MonsterYield.Basic(4)),
            new Monster("buffalo", 6, 0, 20, MonsterYield.Basic(4)),
            new Monster("eagle", 6, 4, 5, MonsterYield.Basic(4)),

            new Monster("bear", 15, 7, 30, MonsterYield.Basic(10)),
            new Monster("whale", 5, 20, 40, MonsterYield.Basic(10)),
            new Monster("puma", 10, 13, 15, MonsterYield.Basic(10)),
            new Monster("wolf", 15, 10, 15, MonsterYield.Basic(10)),
            new Monster("rhino", 10, 10, 30, MonsterYield.Basic(10)),
            new Monster("polar bear", 20, 10, 40, MonsterYield.Basic(10)),
        };

        const double mult = 6d / 5d;

        public static Tile[] tiles = new Tile[]
        {
            new Tile("plains",
                new ActivityYield(new ItemYield[] {
                    //new ItemYield("grass", 1),
                    new ItemYield("berry", 3d * mult),
                    new ItemYield("stick", 1d * mult)
                }, 8),
                new ActivityYield(new ItemYield[] {
                    new ItemYield("rabbit", 1/2d), new ItemYield("rat", 1/2d), new ItemYield("sheep", 1/5d),
                    new ItemYield("buffalo", 1/20d),
                    new ItemYield("wolf", 1/20d),
                }, 10)),

            //new Tile("hills",
            //    new ActivityYield(new ItemYield[] {
            //        new ItemYield("clay", 1),
            //        new ItemYield("root", 1/2d),
            //        new ItemYield("fruit", 1/5d)
            //    }, 1),
            //    new ActivityYield(new ItemYield[] {
            //        new ItemYield("goat", 3/4d), new ItemYield("sheep", 3/4d),
            //        new ItemYield("eagle", 1/20d), new ItemYield("snake", 1/20d), new ItemYield("boar", 1/20d),
            //        new ItemYield("puma", 1/20d),
            //    }, 2)),

            //new Tile("lake",
            //    new ActivityYield(new ItemYield[] {
            //        new ItemYield("weed", 1),
            //        new ItemYield("flower", 1),
            //    }, 1),
            //    new ActivityYield(new ItemYield[] {
            //        new ItemYield("turtle", 1d), new ItemYield("duck", 1d),
            //        new ItemYield("eagle", 1/5d), new ItemYield("buffalo", 1/5d),
            //        new ItemYield("whale", 1/10d),
            //    }, 2)),

           new Tile("mountains",
                new ActivityYield(new ItemYield[] {
                    new ItemYield("rock", 1d),
                    //new ItemYield("root", 1/5d),
                }, 15),
                new ActivityYield(new ItemYield[] {
                    new ItemYield("goat", 1.5), new ItemYield("rat", 1.5),
                    new ItemYield("eagle", 1/2d), new ItemYield("boar", 1/2d),
                    new ItemYield("puma", 1/5d),
                }, 15)),
            
            new Tile("forest",
                new ActivityYield(new ItemYield[] {
                    new ItemYield("branch", 7 * mult),
                    new ItemYield("firewood", 1/10d),
                    //new ItemYield("mushroom", 1/5d),
                    //new ItemYield("nut", 1/5d),
                    new ItemYield("fruit", 1 * mult),
                    //new ItemYield("fruit", 1/2d),
                    //new ItemYield("giant mushroom", 1/10d),
                }, 15),
                new ActivityYield(new ItemYield[] {
                    new ItemYield("rabbit", 2d),
                    new ItemYield("boar", 1), new ItemYield("spider", 1/2d), new ItemYield("snake", 1/2d),
                    new ItemYield("bear", 1/4d), new ItemYield("wolf", 1/4d),
                }, 15)),


            new Tile("snow",
                new ActivityYield(new ItemYield[] {
                    new ItemYield("snow", 1),
                    new ItemYield("stick", 1/2d),
                }, 50),
                new ActivityYield(new ItemYield[] {
                    new ItemYield("penguin", 5d),
                    new ItemYield("polar bear", 2d),
                }, 10)),

            new Tile("desert",
                new ActivityYield(new ItemYield[] {
                    new ItemYield("sand", 1),
                    //new ItemYield("weed", 1/2d),
                }, 50),
                new ActivityYield(new ItemYield[] {
                    new ItemYield("scorpion", 5d),
                    new ItemYield("rhino", 1d),
                }, 10)),
        };

        public static void InitializeConstants()
        {
            List<string[]> newItems = items.ToList();
            foreach (var e in Constants.monsters)
            {
                if(items.Where(f=>f[0] == e.name).Count() == 0)
                    newItems.Add(new string[]{e.name,"butcherable"});
            }
            items = newItems.ToArray();
        }

        public static string[] CheckConstants()
        {
            List<string> msg = new List<string>();

            foreach (Tile t in tiles)
            {
                foreach (ItemYield gather in t.gahtering.items)
                {
                    var a = from n in items
                            where n[0] == gather.item
                            select n;
                    if (a.Count() == 0)
                        msg.Add("Missing item for " + gather.item);
                }

                foreach (ItemYield hunt in t.hunting.items)
                {
                    var a = from n in items
                            where n[0] == hunt.item
                            select n;
                    if (a.Count() == 0)
                        msg.Add("Missing monster " + hunt.item);
                }
            }

            return msg.ToArray();
        }

    }

}