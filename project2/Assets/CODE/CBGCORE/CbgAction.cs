using UnityEngine;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Cbg
{
    public class ActionBase
    {
        public virtual string[] Do(GameData aData){return null;}
        public virtual bool Verify(GameData aData){return true;}

        public string DudeAction(GameData aData, Guid aDudeId, string aAction, Point? aPlace = null)
        {
            Dude d = aData.GetDude(aDudeId);
            d.ActionTakenToday = true;

            aData.ChangeWorld();

            if (aAction == "")
                return "";
            else
                return d.Name + " " + aAction; //TODO place
        }

        public string ConsumeItem(GameData aData, Item aItem, int aCount = 1,string aVerb = "")
        {
            if (aVerb == "")
                aVerb = "consumed";
            aData.Inventory.Remove(aItem);
            return aItem.Name + " " + aVerb + (aCount > 1 ? " x" + aCount : "");
        }
        public string GainItem(GameData aData, Item aItem, int aCount, string aVerb, Guid aDudeId, Point? aPlace, string postfix = "")
        {
            for(int i = 0; i < aCount; i++)
                aData.Inventory.Add(aItem);

            if (postfix != "")
                postfix = " with a " + postfix;

            if (aCount > 0)
                return "you " + aVerb + " " + (aCount == 1 ? "a" : aCount.ToString()) + " " + aItem.Name + postfix;
            else
                return "";
        }
        public string GainExp(GameData aData, Guid aDudeId, double aExp, string aSkill)
        {
            Dude d = aData.GetDude(aDudeId);
            ExpAdd rs = d.skills.AddExp(aSkill, aExp, d.ExpAdjust);
            
            if (rs == ExpAdd.NEW_LEVEL)
                return d.Name + " got better at " + aSkill;
            else
                return "";
        }
        public string DudeDeath(GameData aData, Guid aDudeId, string aDescription)
        {
            Dude d = aData.GetDude(aDudeId);

            if (aData.Dudes[0] == d && GameData.godMode)
                d.HP = 1;
            else
                aData.Dudes.Remove(d);
            
            return "<color=red>" + d.Name + aDescription + "</color>";
        }
    }

    public class MultiAction : ActionBase
    {
        List<ActionBase> mActions = new List<ActionBase>();
        public void AddAction(ActionBase aAction)
        {
            mActions.Add(aAction);
        }
        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();
            foreach(var e in mActions)
                r.AddRange(e.Do(aData));
            return r.ToArray();
        }
        public override bool Verify(GameData aData)
        {
            return mActions.Where(e => e.Verify(aData) != true).Count() == 0;
        }
    }

    public class YieldAndBonus
    {
        public double yield;
        public double adjust;

        public double Total { get { return yield * adjust; } }
    }

    public class ActionGather : ActionBase
    {
        Guid dudeID;
        Point place;
        public ActionGather(Dude aDude, Point aPlace)
        {
            dudeID = aDude.ID;
            place = aPlace;
        }

        public Dictionary<Item, YieldAndBonus> GatherYields(GameData aData)
        {
            Dictionary<Item, YieldAndBonus> ret = new Dictionary<Item, YieldAndBonus>();
            
            Tile t = aData.Map.mMap[place].tile;
            Dude dude = aData.GetDude(dudeID);

            foreach (ItemYield iy in t.gahtering.items)
            {
                Item item = Item.Create(iy.item);

                double gatherAdjust =
                    dude.skills.GetLevel("gathering") +
                    dude.skills.GetLevel(iy.item + " gathering") +
                    dude.skills.GetLevel(t.Name + " gathering");
                gatherAdjust /= 4;

                double chopAdjust =
                    dude.skills.GetLevel("chopping") +
                    dude.skills.GetLevel(iy.item + " chopping") +
                    dude.skills.GetLevel(t.Name + " chopping");
                chopAdjust /= 4;

                bool hasBag = aData.Inventory.HasItem("skin bag");
                bool hasRock = aData.Inventory.HasItem("chopping rock");

                if (item.HasTag("edible") && hasBag)
                    gatherAdjust += .3;  // + 30%

                if (item.HasTag("choppable"))
                {
                    if(hasRock)
                        chopAdjust += .3;  // + 30%

                    ret.Add(item, new YieldAndBonus() { yield = iy.yield, adjust = 1 + chopAdjust });
                }
                else
                {
                    ret.Add(item, new YieldAndBonus() { yield = iy.yield, adjust = 1 + gatherAdjust });
                }

            }

            return ret;
        }

        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();
            r.Add(DudeAction(aData, dudeID, "gathers", place));

            Dude dude = aData.GetDude(dudeID);
            Tile t = aData.Map.mMap[place].tile;

            var hunt = new ActionHunt(dude, place);
            bool dead = hunt.Encounters(r, aData, false);

            if (dead)
                return r.ToArray();

            Dictionary<Item, YieldAndBonus> yield = GatherYields(aData);

            foreach (var kv in yield)
            {
                Item item = kv.Key;
                double yieldTotal = kv.Value.Total;

                //Debug.Log("gather " + item.Name + " " + yieldTotal);
                
                int items_found = Tools.Poisson(yieldTotal);

                r.Add(GainItem(aData, Item.Create(item.Name), items_found, "found", dudeID, place));

                double expGain = 2 * kv.Value.yield;// items_found / kv.Value.adjust;
                
                if (item.HasTag("choppable"))
                    r.Add(GainExp(aData, dudeID, expGain, "chopping"));
                else
                    r.Add(GainExp(aData, dudeID, expGain, "gathering"));

                if(items_found > 0)
                    if (dude.skills.GetLevel("gathering") >= DudeSkillTree.levelBranch)
                        r.Add(GainExp(aData, dudeID, 1, item.Name + " gathering"));
            }
            
            r.Add(GainExp(aData, dudeID, 2, "survival"));
            if (dude.skills.GetLevel("survival") >= DudeSkillTree.levelBranch)
                r.Add(GainExp(aData, dudeID, 1, t.Name + " survival"));

            //r.Add(GainExp(aData, dudeID, 2, "gathering"));
            //if (dude.skills.GetLevel("gathering") >= DudeSkillTree.levelBranch)
            //    r.Add(GainExp(aData, dudeID, 1, t.Name + " gathering"));

            return r.ToArray();
        }

        public override bool Verify(GameData aData)
        {
            return true;
        }
    }

    public class MonsterEncounterStats
    {
        public string name;
        public double encounters;
        public double damagePerEncounter;
    }

    public class ActionHunt : ActionBase
    {
        Guid dudeID;
        Point place;
        public ActionHunt(Dude aDude, Point aPlace)
        {
            dudeID = aDude.ID;
            place = aPlace;
        }

        public DudeStats GetStats(GameData aData)
        {
            Tile t = aData.Map.mMap[place].tile;
            Dude dude = aData.GetDude(dudeID);

            DudeStats huntAdjust = new DudeStats();
            huntAdjust += DudeStats.Attack(1) * dude.skills.GetLevel("hunting");
            huntAdjust += DudeStats.Attack(1) * dude.skills.GetLevel(t.Name + " hunting");

            huntAdjust += DudeStats.Defense(1) * dude.skills.GetLevel("survival");
            huntAdjust += DudeStats.Defense(1) * dude.skills.GetLevel(t.Name + " survival");

            bool hasSpear = aData.Inventory.HasItem("spear");

            if (hasSpear)
            {
                huntAdjust += (DudeStats.Attack(1) + DudeStats.Defense(1)) *
                    (2 + dude.skills.GetLevel("spear hunting"));
            }

            return dude.stats + huntAdjust;
        }
        public DudeStats GetStatsVersus(GameData aData, string monster)
        {
            Dude dude = aData.GetDude(dudeID);
            //Monster m = Constants.monsters.First(mn => mn.name == monster);

            DudeStats monsterAdjust =
                (DudeStats.Attack(1) + DudeStats.Defense(1)) *
                dude.skills.GetLevel(monster + " hunting");

            return GetStats(aData) + monsterAdjust;
        }

        public IEnumerable<KeyValuePair<string, double>> GetMonsters(GameData aData)
        {
            Tile t = aData.Map.mMap[place].tile;

            foreach (ItemYield iy in t.hunting.items)
                yield return new KeyValuePair<string, double>(iy.item, iy.yield);
        }

        public List<MonsterEncounterStats> GetEncounterStats(GameData aData)
        {
            List<MonsterEncounterStats> ret = new List<MonsterEncounterStats>();
            
            //Tile t = aData.Map.mMap[place].tile;
            Dude dude = aData.GetDude(dudeID);

            foreach (var kv in GetMonsters(aData))
            {
                string monsterName = kv.Key;
                double yieldTotal = kv.Value;

                Monster m = Constants.monsters.First(mn => mn.name == monsterName);

                DudeStats statsAdjusted = GetStatsVersus(aData, monsterName);

                FightStats monsterStats = new FightStats(m.stats);
                FightStats dudeStats = new FightStats(
                    statsAdjusted.GetStat(Stat.Attack),
                    statsAdjusted.GetStat(Stat.Defense),
                    dude.HP * 100);

                ret.Add(new MonsterEncounterStats(){ name = monsterName, encounters = yieldTotal,
                    damagePerEncounter = FightStats.AverageHealthLoss(dudeStats, monsterStats)});
            }

            return ret;
        }

        public bool Fight(bool real, bool hunt, GameData aData, List<string> r, out double damage)
        {
            Tile t = aData.Map.mMap[place].tile;
            Dude dude = aData.GetDude(dudeID);
            bool hasSpear = aData.Inventory.HasItem("spear");

            List<string> encounters = new List<string>();

            foreach (var kv in GetMonsters(aData))
            {
                string monsterName = kv.Key;
                double yieldTotal = kv.Value;

                //if (!hunt)
                //    yieldTotal /= 2;    // gathering is not as dangerous

                int yield_num = Tools.Poisson(yieldTotal);

                for (int i = 0; i < yield_num; ++i)
                    encounters.Add(monsterName);
            }

            if(real)
                encounters.Shuffle();

            double hp = dude.HP * 100;
            double initialHp = hp;
            try
            {
                if (!hunt)  // gathering
                {
                    double dmg = Tools.Poisson(t.gahtering.hazards) - Tools.Poisson(dude.GetSkill("survival"));
                    if (dmg < 0)
                        dmg = 0;

                    hp -= dmg;
                    
                    if (hp <= 0)
                    {
                        if(real)
                            r.Add(DudeDeath(aData, dude.ID, " fell into a bottomless pit"));
                        return true;
                    }

                    return false;
                }
                
                foreach (string monsterName in encounters)
                {
                    Monster m = Constants.monsters.First(mn => mn.name == monsterName);

                    DudeStats statsAdjusted = GetStatsVersus(aData, monsterName);

                    FightStats monsterStats = new FightStats(m.stats);
                    FightStats dudeStats = new FightStats(
                        statsAdjusted.GetStat(Stat.Attack),
                        statsAdjusted.GetStat(Stat.Defense),
                        hp);

                    if (hunt)
                        FightStats.Fight(dudeStats, monsterStats);
                    else
                    {
                        for (int i = 0; i < 2; ++i)
                            FightStats.Attack(monsterStats, dudeStats);
                    }

                    hp = dudeStats.hp;

                    if (!real)
                    {
                        if (hp <= 0)
                            return true;
                    }
                    else
                    {
                        if (hp <= 0)
                        {
                            r.Add(DudeDeath(aData, dude.ID, " was mauled by a " + monsterName));
                            return true;
                        }
                        else if (hunt)
                        {
                            if (monsterStats.hp <= 0)
                            {
                                string postfix = "";
                                if (hasSpear)
                                    postfix = "spear";
                                r.Add(GainItem(aData, Item.Create(monsterName), 1, "killed", dude.ID, place, postfix));

                                if (dude.skills.GetLevel("hunting") >= DudeSkillTree.levelBranch)
                                    r.Add(GainExp(aData, dude.ID, 1, monsterName + " hunting"));
                            }
                            else
                                r.Add("Fought a " + monsterName + " but couldn't kill it.");
                        }
                        else // gather
                        {
                            r.Add("Ran away from an angry " + monsterName);
                        }

                    }

                }

            }
            finally
            {
                if (hp < 0)
                    hp = 0;
                if (real)
                    dude.HP = hp / 100;
                damage = initialHp - hp;
            }

            return false;
        }

        public bool Encounters(List<string> r, GameData aData, bool hunt)
        {
            //Tile t = aData.Map.mMap[place].tile;
            Dude dude = aData.GetDude(dudeID);
            bool hasSpear = aData.Inventory.HasItem("spear");

            double damage;
            bool dead = Fight(true, hunt, aData, r, out damage);

            if (dead)
                return true;

            if (hunt & hasSpear)
            {
                if (dude.skills.GetLevel("hunting") >= DudeSkillTree.levelBranch)
                    r.Add(GainExp(aData, dude.ID, 1, "spear hunting"));
            }

            return false;
        }
        
        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();

            r.Add(DudeAction(aData,dudeID,"hunts",place));

            Dude dude = aData.GetDude(dudeID);
            Tile t = aData.Map.mMap[place].tile;

            bool dead = Encounters(r, aData, true);

            if (dead)
                return r.ToArray();

            r.Add(GainExp(aData, dudeID, 4, "survival"));
            if (dude.skills.GetLevel("survival") >= DudeSkillTree.levelBranch)
                r.Add(GainExp(aData, dudeID, 1, t.Name + " survival"));

            r.Add(GainExp(aData, dudeID, 2, "hunting"));
            if (dude.skills.GetLevel("hunting") >= DudeSkillTree.levelBranch)
                r.Add(GainExp(aData, dudeID, 1, t.Name + " hunting"));

            //for (int i = 0; i < (t.hunting.hazards - survivalAdjust); ++i)
            //{
            //    if(Tools.Chance(.5f))
            //        dude.health -= .05f;
            //}
            
            ////bad things happen
            //if (dude.health < 0)
            //    r.Add(DudeDeath(aData, dudeID, " was mauled by a bear"));
            //else if (Tools.Chance((t.hunting.hazards - survivalAdjust) * 0.02f))
            //    r.Add(DudeDeath(aData, dudeID, " was overrun by spiders"));

            return r.ToArray();
        }
        
        public override bool Verify(GameData aData)
        {
            return true;
        }
    }

    public class ActionButcher : ActionBase
    {
        Guid dudeID;
        ItemSet items;

        public ActionButcher(Dude aDude, Item aButcher)
        {
            items = new ItemSet(aButcher);
            dudeID = aDude.ID;
        }

        public ActionButcher(Dude aDude, ItemSet aItems)
        {
            items = aItems;
            dudeID = aDude.ID;
        }

        public Item ToButcher //returns null if we have nothing butcherable selected
        {
            get
            {
                try{return items.items.Where(e => e.item.Tags.Contains("butcherable")).First().item;}
                catch{return null;}
            }
        }

        public Dictionary<string, double> ButcherYields(GameData aData)
        {
            Dictionary<string, double> ret = new Dictionary<string, double>();
            var toButcher = ToButcher;
            Monster m = Constants.monsters.First(mn => mn.name == toButcher.Name);
            Dude dude = aData.GetDude(dudeID);
            double butcherAdjust = dude.skills.GetLevel("butchering") + dude.skills.GetLevel(toButcher.Name + " butchering");
            butcherAdjust /= 5;

            foreach (string itemName in m.yield.items.Keys)
                ret.Add(itemName, m.yield.items[itemName] * (1 + butcherAdjust));

            return ret;
        }

        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();

            Dude dude = aData.GetDude(dudeID);
            var toButcher = ToButcher;
            //Monster m = Constants.monsters.First(mn => mn.name == toButcher.Name);
            r.Add(DudeAction(aData, dudeID, "butchers"));
            r.Add(ConsumeItem(aData, toButcher));

            Dictionary<string, double> butcherYields = ButcherYields(aData);

            foreach (var kv in butcherYields)
            {
                int yieldCount = Tools.Poisson(kv.Value);
                r.Add(GainItem(aData, Item.Create(kv.Key), yieldCount, "butchered some ", dudeID, null));
            }

            r.Add(GainExp(aData, dudeID, 2, "butchering"));
            if (dude.skills.GetLevel("butchering") >= DudeSkillTree.levelBranch)
                r.Add(GainExp(aData, dudeID, 1, toButcher.Name + " butchering"));
            
            return r.ToArray();
        }
        
        public override bool Verify(GameData aData)
        {
            
            return true;
        }
    }

    public class ActionCraft : ActionBase
    {
        Guid dudeID;
        Cbg.Recipe recipe;
        ItemStack[] ingredients;

        public ActionCraft(Dude aDude, Cbg.Recipe aRecipe, ItemStack[] aIngredients)
        {
            recipe = aRecipe;
            dudeID = aDude.ID;
            ingredients = aIngredients;
        }
        
        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();
            r.Add(DudeAction(aData,dudeID,"crafts"));

            Dude d = aData.GetDude(dudeID);

            foreach (var e in TryCraft())
            {
                if(!e.Tags.Contains("permanent"))
                    r.Add(ConsumeItem(aData, e));
            }

            foreach (var e in recipe.output.items)
            {
                double adjust = e.count * d.GetSkill("crafting") / 5d;
                int newCount = e.count + Tools.Poisson(adjust);
                r.Add(GainItem(aData, e.item, newCount, "crafted", dudeID, null));
            }

            r.Add(GainExp(aData, dudeID, 4, "crafting"));

            return r.ToArray();
        }

        bool TryCraftOn(TagGroup[] itemTagGroups, out List<Item> use)
        {
            use = new List<Item>();

            List<ItemStack> tempIngd = (from ing in ingredients
                                        select new ItemStack(ing.item, ing.count)).ToList();
            //go through each tag set
            foreach (TagGroup craft in itemTagGroups)
            {
                //for each tag in the set, check if we have this item available
                bool itemOk = false;
                int count = craft.count;
                foreach (ItemStack st in tempIngd)
                {
                    if ((from g in craft.tags
                         where !st.item.Tags.Contains(g)
                         select g).Any() == false) // all the tags match
                    {
                        if (st.count >= count)
                        {
                            for (int i = 0; i < count; i++)
                                use.Add(st.item);

                            st.count -= count;
                            itemOk = true;
                            break;
                        }
                        else
                        {
                            for (int i = 0; i < st.count; i++)
                                use.Add(st.item);

                            count -= st.count;
                            st.count = 0;
                        }
                    }
                }
                if (!itemOk)
                    return false;
            }
            return true;
        }

        public Item[] TryCraft()
        {
            //TODO do first pass to make sure every individual item is craftable??? nah, screw that

            PermutationFinder<TagGroup> pmf = new PermutationFinder<TagGroup>();
            List<Item> use = null;
            //List<ItemStack> tempIngd = null;
            if(pmf.Evaluate(recipe.input.tagGroups,
                itemTagGroups => {return TryCraftOn(itemTagGroups, out use);}
            ))
                return use.ToArray();
            return null;
        }

        public override bool Verify(GameData aData)
        {

            return true;
        }
    }

    public class ActionSleep : ActionBase
    {
        Guid dudeID;
        int times = 1;

        public ActionSleep(Dude aDude, bool longSleep_)
        {
            dudeID = aDude.ID;
            if (longSleep_)
                times = 1;
        }
        
        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();

            r.Add(DudeAction(aData, dudeID, ""));

            var dude = aData.Dudes.First(e => e.ID == dudeID);
            double healthGain = dude.RestHP * times;

            UnityEngine.Debug.Log(dude.Name + " health " + healthGain);

            healthGain = Tools.Poisson(healthGain);

            double oldHealth = dude.HP;
            dude.HP += healthGain/100;


            double diff = dude.HP - oldHealth;
            diff *= 100;
            r.Add(GainExp(aData, dudeID, (diff / 10 * 2), "survival"));  // 2 exp for 10 health recovered

            if (diff >= 1)
                r.Add(dude.Name + " rested and gained " + diff.ToString("0") + " hp");
            return r.ToArray();
        }
        public override bool Verify(GameData aData)
        {
            return true;
        }
    }

    public class ActionAdvanceDay : ActionBase
    {
        public ActionAdvanceDay()
        {
        }

        void Rest(GameData aData, List<string> output)
        {
            MultiAction sleep = new MultiAction(); 
            foreach (Dude d in aData.Dudes)
                sleep.AddAction(new ActionSleep(d, false));
            output.AddRange(sleep.Do(aData));
            //output.Add("everyone rests");
        }

        public static Dictionary<Dude, int> AssignFoodDistribution(Inventory inv, List<Dude> dudes)
        {
            Dictionary<Dude, int> dudeFood = new Dictionary<Dude, int>();

            foreach (var d in dudes)
                dudeFood.Add(d, 0);

            for (int food = 1; food <= 6; ++food)
            {
                foreach (Dude d in dudes.OrderBy(e => e.Satiation))
                {
                    if (d.FoodPerDay < food)
                        continue;

                    Item foodItem = inv.TryGetItemByTags("edible");
                    if (foodItem != null)
                    {
                        inv.Remove(foodItem);
                        dudeFood[d]++;
                    }
                    else
                        return dudeFood;
                }

            }

            return dudeFood;
        }
        
        void Eat(GameData aData, List<string> output)
        {
            Inventory inv = aData.Inventory;
            Dictionary<Dude, int> dudeFood = AssignFoodDistribution(inv, aData.Dudes);

            foreach (Dude d in dudeFood.Keys)
            {
                d.FoodAdvance(dudeFood[d]);
                if (dudeFood[d] > 0)
                    output.Add(d.Name + " eats x" + dudeFood[d]);
                else
                    output.Add(d.Name + " goes hungry");
            }

            //List<Dude> dudes = aData.Dudes.OrderBy(e => e.Satiation).ToList();
            //List<string> hungryDudes = new List<string>();
            //List<string> eatDudes = new List<string>();
            //List<string> eatitems = new List<string>();

            //foreach (Dude d in dudes)
            //{
            //    Inventory inv = aData.Inventory;
            //    Item food = inv.TryGetItemByTags("edible");
            //    if (food != null)
            //    {
            //        d.Satiation += .15f;
            //        //output.Add(d.Name + " eats a " + food.Name);
            //        inv.Remove(food);
            //        eatDudes.Add(d.Name);
            //        eatitems.Add(food.Name);
            //    }
            //    else
            //    {
            //        d.Satiation -= .1f;
            //        hungryDudes.Add(d.Name);
            //        //output.Add(d.Name + " did went hungry");
            //    }
            //}

            //if (eatDudes.Count > 0)
            //{

            //    if(hungryDudes.Count == 0)
            //        output.Add("everyone ate " + Tools.CombineWords(eatitems));
            //    else
            //        output.Add(Tools.CombineWords(eatDudes) + " ate " + Tools.CombineWords(eatitems));
            //}
            //if (hungryDudes.Count > 0)
            //{
            //    if(eatDudes.Count == 0)
            //        output.Add("everyone went hungry");
            //    else
            //        output.Add(Tools.CombineWords(hungryDudes) + " went hungry");
            //}

        }
        
        public override string[] Do(GameData aData)
        {
            List<string> r = new List<string>();
            aData.GameTime+=1;

            HandleDamage(aData, r);
            HandleFire(aData, r);
            Eat(aData, r);
            HandleItemDecay(aData, r);
            CheckFatality(aData, r);
            Rest(aData, r);


            //randomly get a new dude every .. days
            //if (aData.GameTime % Formulas.daysPerDude == 0 && aData.Dudes.Count() < 5)
            if (Tools.Chance(1d / Formulas.daysPerDude) && aData.Dudes.Count() < 5)
            {
                Dude nd = new Dude();
                aData.Dudes.Add(nd);
                r.Add("<color=green>" + nd.Name + " migrated to your settlement"+"</color>");
            }

            foreach (var e in aData.Dudes)
                e.ActionTakenToday = false;

            //r.Add("---------------------------------------");
            r.Add("---------"+"you advanced to day " + aData.GameTime+"---------");



            return r.ToArray();
        }

        public override bool Verify(GameData aData)
        {
            return true;
        }

        void HandleDamage(GameData aData, List<string> output)
        {
            List<Dude> deadDudes = new List<Dude>();
            foreach (Dude d in aData.Dudes)
            {
                if(Tools.Chance(Cbg.Formulas.DeathChance(d)))
                    deadDudes.Add(d);
            }
            foreach (var e in deadDudes)
                output.Add(DudeDeath(aData,e.ID," died from bad health."));
        }

        void CheckFatality(GameData aData, List<string> output)
        {
            foreach (Dude d in aData.Dudes.ToArray())
            {
                if (d.Satiation == 0)
                    output.Add(DudeDeath(aData, d.ID, " starved to death."));
                else if(d.Warmth == 0)
                    output.Add(DudeDeath(aData, d.ID, " froze to death."));
            }
                
        }
        
        public const double ONE_LOG_HEAT = 1;
        public const double TWO_LOG_HEAT = .75;
        //public const double THREE_LOG_HEAT = .5;
        public const double FIRE_BONUS = 1;
        
        static public void HeatProduction(Inventory inv, int logMax, out double heat, out int logsBurned, out int firesLit)
        {
            heat = 0;
            logsBurned = 0;
            firesLit = 0;
            
            bool stop = false;

            for (int j = 0; j < 2; ++j)
            {
                for (int i = 0; i < inv.ItemCount("fireplace"); ++i)
                {
                    if (logsBurned == logMax)
                    {
                        stop = true;
                        break;
                    }

                    Item wd = inv.TryGetItemByTags("firewood");
                    if (wd == null)
                    {
                        stop = true;
                        break;
                    }

                    inv.Remove(wd);
                    logsBurned += 1;

                    if (j == 0)
                        firesLit++;

                    if (j == 0)
                        heat += ONE_LOG_HEAT;
                    else if(j == 1)
                        heat += TWO_LOG_HEAT;
                    //else
                    //    heat += THREE_LOG_HEAT;
                }

                if (stop)
                    break;
            }
        }

        void HandleItemDecay(GameData aData,List<string> output)
        {
            List<ItemStack> toConsume = new List<ItemStack>();
            foreach (var e in aData.Inventory.Items)
            {
                int numConsumed = 0;
                for(int i = 0; i < e.count; i++)
                    numConsumed += Tools.Chance(Cbg.Formulas.itemDecayChance) ? 1 : 0;
                toConsume.Add(new ItemStack(e.item,numConsumed));
            }
            foreach(var e in toConsume)
                output.Add(ConsumeItem(aData,e.item,e.count,"decayed"));
        }
          
        void HandleFire(GameData aData,List<string> output)
        {
            Inventory inv = aData.Inventory;
            int logMax = aData.FireConsume;
            
            double heat;
            int logsBurned;
            int firesLit;

            HeatProduction(inv, logMax, out heat, out logsBurned, out firesLit);

            List<Dude> dudes = aData.Dudes.OrderBy(e => e.Warmth).ToList();

            for(int i = 0; i < dudes.Count; ++i)
            {
                double dudeHeat = heat;
                if (i < firesLit)
                    dudeHeat += 1;

                dudes[i].HeatAdvance(dudeHeat);
            }
            if (logsBurned == 0)
                output.Add("everyone is cold");
            else
                output.Add("Fires burn using " + logsBurned + " logs for " + heat + " heat");


        //    bool burn = false;
        //    List<Dude> dudes = aData.Dudes.OrderBy(e => e.Warmth).ToList();

        //    int blanketCount = aData.Inventory.ItemCount("blanket");
        //    for (int i = 0; i < aData.Inventory.ItemCount("fireplace"); i++)
        //    {
        //        if(aData.Inventory.ItemCount("firewood") > 0)
        //        {
        //            output.Add(ConsumeItem(aData,aData.Inventory.GetItem("firewood")));
        //            output.Add("the fire burns");
        //            foreach(var e in dudes.TakeWhile((dude,index)=>index < 2))
        //            {
        //                if(blanketCount-- > 0)
        //                {
        //                    output.Add(e.Name + " is very warm");
        //                    e.Warmth += .2f;
        //                }
        //                else
        //                {
        //                    output.Add(e.Name + " is warm");
        //                    e.Warmth += .1f;
        //                }
        //            }
        //            dudes.RemoveAt(0);
        //            if(dudes.Count > 0)
        //                dudes.RemoveAt(0);
        //            burn = true;
        //        }
        //    }
        //    if(!burn)
        //    {
        //        foreach(var e in aData.Dudes)
        //            e.Warmth -= .05f;
        //        output.Add("everyone is cold");
        //    }
        }
    }
}
