﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CbgGameManager : MonoBehaviour {
    public static CbgGameManager Inst{ get; private set; }
    
    public Cbg.GameData Data { get; private set; }

    public CbgMapView MapView{ get; private set; }
    public CbgDudeView DudeView{ get; private set; }
    public CbgInventoryView InventoryView{ get; private set; }
    public CbgMessageView MessageView{ get; private set; }
    public CbgActionView ActionView{ get; private set; }
    
    void Start () {
        Inst = this;
        InitializeNewGame();
    }

    void InitializeNewGame()
    {
        Cbg.Constants.InitializeConstants();


        //initalize data
        Data = new Cbg.GameData();
        Data.Inventory.Add(Cbg.Item.Create("berry"), 20);
        //Data.Inventory.Add(Cbg.Item.Create("wood"));
        Data.Inventory.Add(Cbg.Item.Create("fireplace"));
        Data.Inventory.Add(Cbg.Item.Create("firewood"), 1);
        Data.Inventory.Add(Cbg.Item.Create("stick"), 10);
        //Data.Inventory.Add(Cbg.Item.Create("spear"));
        //Data.Inventory.Add(Cbg.Item.Create("axe"));
        //Data.Inventory.Add(Cbg.Item.Create("basket"));

        for (int i = 0; i < 5; ++i)
            Data.Dudes.Add(new Cbg.Dude());

        Data.Map = Cbg.WorldMap.Generate(new Cbg.Point(10, 10));


        //initialize the views
        MapView = gameObject.AddComponent<CbgMapView>();
        DudeView = gameObject.AddComponent<CbgDudeView>();
        InventoryView = gameObject.AddComponent<CbgInventoryView>();
        MessageView = gameObject.AddComponent<CbgMessageView>();
        ActionView = gameObject.AddComponent<CbgActionView>();

        foreach (var s in Cbg.Constants.CheckConstants())
            MessageView.AddMessage(s);
    }

    public void DoAction(Cbg.ActionBase aAction)
    {
        if (aAction.Verify(Data))
        {
            //TODO log message
            var output = aAction.Do(Data);
            foreach(var e in output)
                MessageView.AddMessage(e);
        }
    }
    void Update()
    {
        if (Data.Dudes.Count() == 0)
            MessageView.AddMessage("YOU LOSE");
        else if (Data.Dudes.Where(e => !e.ActionTakenToday).Count() == 0)
        {
            DoAction(new Cbg.ActionAdvanceDay());
            DudeView.ResetSelection();
            InventoryView.ResetSelection();
        }
    }
    
}

    