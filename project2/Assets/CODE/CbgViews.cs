﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class CbgMapView : MonoBehaviour
{
    Cbg.Point? mSelectedPoint = null;
    Cbg.WorldMap Map { get { return CbgGameManager.Inst.Data.Map; } }

    Cbg.FightSimulator sim = new Cbg.FightSimulator();

    //TODO initialize/update with world data when it changes

    void Start () {
    }
    void Update () {
        
    }

    void DrawMap()
    {
        int padding = 3;
        int tooltipHeight = 20;
        int dim = (Cbg.Constants.mapWidth - padding * (Map.size.x) - tooltipHeight)/ Map.size.x;

        foreach (Cbg.Point p in Cbg.Point.Range(Map.size))
        {
            Cbg.WorldMapTile tl = Map.mMap[p];

            string mark = tl.tile.Name[0].ToString();


            GUIStyle style = GUI.skin.button;
            if (mSelectedPoint.HasValue && mSelectedPoint == p)
            {
                style = new GUIStyle(GUI.skin.button);
                style.normal.textColor = Color.red;
                style.hover.textColor = Color.red;
            }

            if (GUI.Button(new Rect(padding + p.x * (padding + dim), padding + p.y * (padding + dim), dim, dim),
                new GUIContent(mark, p.ToString()), style))
            {
                if (mSelectedPoint.HasValue && mSelectedPoint == p)
                    mSelectedPoint = null;
                else
                    mSelectedPoint = p;
            }
        }




        string display= "";
        if (GUI.tooltip != "")
        {
            var pos = Cbg.Point.FromString(GUI.tooltip);
            display = Map.mMap [pos].tile.Name;
            SetGui(pos);
        } else if (GUI.tooltip == "" && GetLocationSelected() != null)
            display = CbgGameManager.Inst.Data.Map.mMap [GetLocationSelected().Value].tile.Name;
        GUI.Label(new Rect(padding, padding + (dim + padding) * Map.size.y, Cbg.Constants.mapWidth, tooltipHeight), display);

    }

    void SetGui(Cbg.Point location)
    {
        var gm = CbgGameManager.Inst;

        var selectedDude = gm.Data.GetDude(gm.DudeView.GetSelectedDude());
        if (selectedDude == null)
            return;

        var a = new Cbg.ActionHunt(selectedDude, location);

        string ret = "";
        {
            Cbg.FightResult fr = sim.Get(gm.Data.WorldState, location, selectedDude, false);
            
            ret += "Gather:\n";
            ret += "Death: " + P(fr.deathRatio.Get) + "\n";
            ret += "Nonlethal damage: " + D(fr.nonlethalDamage.Get) + "\n";
            var gather = new Cbg.ActionGather(selectedDude, location);
            ret += CbgMessageView.YieldData(gather.GatherYields(gm.Data));
        }

        ret += "\n\n";

        {
            Cbg.FightResult fr = sim.Get(gm.Data.WorldState, location, selectedDude, true);

            ret += "Hunt:\n";
            ret += "Death: " + P(fr.deathRatio.Get) + "\n";
            ret += "Nonlethal damage: " + D(fr.nonlethalDamage.Get) + "\n";
            ret += ParseEncountersData(a.GetEncounterStats(gm.Data));
        }


        CbgGameManager.Inst.MessageView.Tooltip = ret;
    }

    void OnGUI()
    {
        DrawMap();
    }

    public Cbg.Point? GetLocationSelected()
    {
        return mSelectedPoint;
    }

    public void ResetSelection()
    {
        mSelectedPoint = null;
    }

    static public string D(double d) { return ((double)(int)(d * 100)) / 100 + ""; }
    static public string P(double d) { return (int)(d * 100) + "%"; }

    static string ParseEncountersData(List<Cbg.MonsterEncounterStats> lst)
    {
        //double health = 1;
        //var gm = CbgGameManager.Inst;
        //var selectedDude = gm.Data.GetDude(gm.DudeView.GetSelectedDude());
        //if (selectedDude != null)
        //    health = selectedDude.HP;

        string ret = "";

        //double fatalEncounter = 0;
        //double healthLoss = 0;

        foreach (var st in lst)
        {
            //if (st.damagePerEncounter >= health*100)
            //    fatalEncounter += st.encounters;
            //else
            //    healthLoss += st.damagePerEncounter * st.encounters;

            ret += st.encounters.ToString("0.0") + " " + st.name + "s -" + st.damagePerEncounter.ToString("0.0") + " each\n";
        }

        return ret;
    }

}

public class CbgDudeView : MonoBehaviour
{
    List<Cbg.Dude> Dudes { get { return CbgGameManager.Inst.Data.Dudes; } }
    Cbg.Dude selectedDude = null;

    void Start ()
    {
        ResetSelection();
    }
    void Update () {
        if (selectedDude != null && selectedDude.ActionTakenToday)
        {
            if (Cbg.GameData.godMode && Dudes[0] == selectedDude )
            { }
            else
                ResetSelection();
        }
    }

    void DrawDudes()
    {

        Cbg.Point padding = new Cbg.Point(5, 5);
        Cbg.Point size = new Cbg.Point((Cbg.Constants.dudeBoxWidth - padding.x*2)/3, 60);
        Cbg.Point offset = new Cbg.Point(padding.x,padding.y + Cbg.Constants.topSectionHeight + Cbg.Constants.guiPadding);

        int counter = 0;

        foreach (Cbg.Dude dude in Dudes)
        {
            Cbg.Point pos = new Cbg.Point(offset.x, offset.y + counter * (padding.y + size.y));

            {
                GUIStyle style = GUI.skin.button;
                if (selectedDude == dude)
                {
                    style = new GUIStyle(GUI.skin.button);
                    style.normal.textColor = Color.red;
                    style.hover.textColor = Color.red;
                }

                if(dude.ActionTakenToday)
                {
                    style = new GUIStyle(GUI.skin.button);
                    style.normal.textColor = Color.gray;
                    style.hover.textColor = Color.gray;
                }

                if (GUI.Button(new Rect(pos.x, pos.y, size.x, size.y), new GUIContent(dude.Name), style))
                {
                    if(!dude.ActionTakenToday)
                        selectedDude = dude;
                }

                pos.x += padding.x + size.x;
            }

            GUIStyle buttonStyle = GUI.skin.button;
            {
                string buttonText = 
                    "health: " + (int)(dude.HP * 100) + "\n" 
                    + "warmth: " + (int)(dude.Warmth * 100) + "\n"
                    + "satiation: " + (int)(dude.Satiation * 100) + " (" + dude.FoodPerDay + ")";
                string tooltipText = buttonText + "\nchance of death: " + (int)(Cbg.Formulas.DeathChance(dude)*100) + "%\n";
                tooltipText += "Food per day: " + dude.FoodPerDay + "\n";
                tooltipText += "Exp adjustment: " + (int)(dude.ExpAdjust*100) + "%\n";
                tooltipText += "Rest HP recover: " + CbgMapView.D(dude.RestHP) + "\n";
                if (GUI.Button(new Rect(pos.x, pos.y, size.x, size.y), new GUIContent(buttonText, tooltipText), buttonStyle))
                    dude.FoodPerDay++;
                
                pos.x += padding.x + size.x;
            }

            {
                string buttonText = "";
                string tooltipText = "";
                int linecnt = 0;
                foreach (var kv in dude.skills.GetAllSkills())
                {
                    string skill = kv.Key + " ";
                    for (int i = 0; i < kv.Value; ++i)
                        skill += "*";


                    if (kv.Value > 0)
                    {
                        if (++linecnt < 4)
                            buttonText += skill + "\n";

                        if (linecnt == 4)
                        {
                            buttonText += "...";
                        }
                    }

                    skill += " (" + dude.skills.GetExp(kv.Key) + " exp)";
                    tooltipText += "\n" + skill;
                }

                if (buttonText == "")
                {
                    buttonText = "no skills";
                    //tooltipText = dude.Name + " has no skills";
                }

                buttonStyle.alignment = TextAnchor.MiddleLeft;
                GUI.Label(new Rect(pos.x, pos.y, size.x, size.y), new GUIContent(buttonText,tooltipText), buttonStyle);
                //pos.x += padding.x + size.x;

            }
            ++counter;
        }

        //if (showTooltip)
        {
            CbgGameManager.Inst.MessageView.Tooltip = GUI.tooltip;
            /*GUIStyle style = new GUIStyle();
            style.normal.background = UnityUtilities.GetColorTexture(Color.black);
            style.normal.textColor = Color.white;
            var dim = style.CalcSize(new GUIContent(GUI.tooltip));
            GUI.Label(new Rect(Input.mousePosition.x, Screen.height-Input.mousePosition.y, dim.x,dim.y),GUI.tooltip,style);*/
        }
    }

    void OnGUI()
    {
        DrawDudes();
    }

    public System.Guid GetSelectedDude()
    {
        if (!Dudes.Contains(selectedDude))
            ResetSelection();
        
        if(selectedDude == null)
			return  System.Guid.Empty;
        return selectedDude.ID;
    }

    public void ResetSelection()
    {
        selectedDude = null;
        foreach(Cbg.Dude d in Dudes)
            if (!d.ActionTakenToday)
            {
                selectedDude = d;
                return;
            }
    }
}

public class CbgInventoryView : MonoBehaviour
{
    HashSet<string> selectedItems = new HashSet<string>();

    Cbg.Inventory Inventory { get { return CbgGameManager.Inst.Data.Inventory; } }

    //TODO initialize/update with world data when it changes

    void Start()
    {
    }

    void Update ()
    {

    }

    void DrawInventory()
    {
        Cbg.Point size = new Cbg.Point(80, 40);
        Cbg.Point padding = new Cbg.Point(5, 5);
        Cbg.Point offset = new Cbg.Point(Cbg.Constants.dudeBoxWidth + Cbg.Constants.guiPadding, Cbg.Constants.topSectionHeight + Cbg.Constants.guiPadding);

        int buttonsInRow = 10;

        Cbg.Point counter = new Cbg.Point(0, 0);
        foreach (Cbg.ItemStack st in Inventory.Items)
        {
            Cbg.Tools.Assert(st.count >= 0);

            if (st.count == 0)
                continue;

            string buttonText = st.item.Name;
            if (st.count > 1)
                buttonText += "\nx" + st.count;

            if(st.item.HasTag("edible"))
                buttonText = "<color=green>" + buttonText + "</color>";
            //if (st.item.HasTag("choppable"))
            //    buttonText = "<color=brown>" + buttonText + "</color>";
            if (st.item.Name == "firewood")
                buttonText = "<color=brown>" + buttonText + "</color>";

            GUIStyle style = GUI.skin.button;
            if (selectedItems.Contains(st.item.Name))
            {
                style = new GUIStyle(GUI.skin.button);
                style.normal.textColor = Color.red;
                style.hover.textColor = Color.red;
                style.richText = false;
            }

            Cbg.Point corner = offset + padding + Cbg.Point.Scale(counter, padding + size);

            string tooltip = String.Join(", ", st.item.Tags);

            if (GUI.Button(new Rect(corner.x, corner.y, size.x, size.y), new GUIContent(buttonText, tooltip), style))
            {
                if (selectedItems.Contains(st.item.Name))
                    selectedItems.Remove(st.item.Name);
                else
                    selectedItems.Add(st.item.Name);
                
            }

            ++counter.x;
            if (counter.x >= buttonsInRow)
            {
                ++counter.y;
                counter.x = 0;
            }
        }
        Cbg.Point clearCorner = offset + padding + Cbg.Point.Scale(counter, padding + size);
        if(selectedItems.Any())
            if (GUI.Button(new Rect(clearCorner.x, clearCorner.y + size.y / 8, size.x * 2 / 3, size.y * 3 / 4), new GUIContent("clear", "<i>clear selection</i>")))
                selectedItems.Clear();

        CbgGameManager.Inst.MessageView.Tooltip = GUI.tooltip;
    }

    void OnGUI()
    {
        DrawInventory();
    }

    public Cbg.ItemSet GetSelectedItems()
    {
        Dictionary<string, Cbg.ItemStack> ret = new Dictionary<string, Cbg.ItemStack>();


        foreach (string name in selectedItems.ToArray())
            if (Inventory.HasItem(name))
                ret.Add(name, Inventory.GetItemStack(name));
            else
                selectedItems.Remove(name);

        Cbg.ItemSet r = new Cbg.ItemSet("");
        r.items = ret.Values.ToArray();
        return r;
    }

    public void ResetSelection()
    {
        selectedItems.Clear();
    }
}

public class CbgActionView : MonoBehaviour
{
    //available actions may depend on dude skill???
    //setup callbacks for submit

    bool isCraftView = false;

    void Start () {
        
    }
    void Update () {
        
    }

    class ActionData
    {
        public Cbg.ActionCraft craft = null;
        public Cbg.ActionButcher butcher = null;
        public string error = "";
    }

    static ActionData GetCraft(Cbg.Recipe aRecipe, CbgGameManager gm, Cbg.Dude selectedDude, Cbg.Point? selectedTile)
    {
        ActionData ret = new ActionData();

        if (selectedDude != null)
        {
            IEnumerable<Cbg.ItemStack> selection = gm.InventoryView.GetSelectedItems().items;
            if (selection.Count() == 0)
                selection = gm.Data.Inventory.Items.ToArray();
            ret.craft = new Cbg.ActionCraft(selectedDude, aRecipe, selection.ToArray());

            Cbg.Item[] itemsUsed = ret.craft.TryCraft();
            if (itemsUsed != null)
            {
                if (aRecipe.output.items[0].item.Name != "butcher")
                    return ret; 
                else
                {
                    Cbg.Tools.Assert(itemsUsed.Length == 1);
                    ret.butcher = new Cbg.ActionButcher(selectedDude, itemsUsed.First());
                }
            }
            else
                ret.error = "Error: you do not have the right ingredients";
        }
        else
            ret.error = "Error: no dude selected";

        return ret;
    }

    void OnGUI()
    {
        int padding = 5;
        //int startx = Cbg.Constants.dudeBoxWidth + Cbg.Constants.guiPadding;
        //int starty = Cbg.Constants.topSectionHeight + Cbg.Constants.guiPadding + Cbg.Constants.inventoryBoxHeight;
        int startx = Cbg.Constants.messageBoxWidth + Cbg.Constants.mapWidth + Cbg.Constants.guiPadding;
        int starty = Cbg.Constants.guiPadding; 
        //int totalWidth = Cbg.Constants.actionBoxWidth;
        int width = 80;
        int height = 40;

        var gm = CbgGameManager.Inst;
		var selectedDude = gm.Data.GetDude(gm.DudeView.GetSelectedDude());
		var selectedTile = gm.MapView.GetLocationSelected();		

        var actions = Cbg.Constants.actions;
        for(int i = 0; i < actions.Length; i++)
        {
            string tooltip = "";//Cbg.Constants.actions[i];
            string name = actions[i];

            if (actions[i] == "fire"){
                tooltip = "Fire burns " + gm.Data.FireConsume + " per day";
                name += " (" + gm.Data.FireConsume + ")";
            } else if (actions[i] == "cook")
                tooltip = "cook";

            if (GUI.Button(new Rect(startx + padding + i * (padding + width), starty, width, height),
                new GUIContent(name, tooltip)))
            {
                Cbg.ActionBase action = null;
                if(actions[i] == "gather" && selectedDude != null && selectedTile != null)
                    action = new Cbg.ActionGather(selectedDude,selectedTile.Value);
                else if (actions[i] == "hunt" && selectedDude != null && selectedTile != null)
                    action = new Cbg.ActionHunt(selectedDude,selectedTile.Value);
                else if (actions[i] == "craft")
                    isCraftView = !isCraftView;
                else if (actions[i] == "rest" && selectedDude != null)
                    action = new Cbg.ActionSleep(selectedDude, true);
                else if (actions[i] == "advance")
                    action = new Cbg.ActionAdvanceDay();
                else if (actions[i] == "cook")
                {
                    var butcher = new Cbg.ActionButcher(selectedDude,gm.InventoryView.GetSelectedItems());
                    if(butcher.ToButcher == null)
                    {
                        gm.MessageView.AddMessage("Can't cook as no butcherable items were selected");
                        gm.MessageView.AddMessage("TODO cooking should work with all items but right now it's just another name for butcher");
                        action = null;
                    }
                    else
                        action = butcher;
                }
                else if (actions[i] == "fire")
                {
                    gm.Data.FireConsume++;
                    continue;
                }

                if(actions[i] != "craft")
                    isCraftView = false;

				if(action != null)
                    gm.DoAction(action);
                else if(!isCraftView)
                {
                    gm.MessageView.AddMessage("Error: can not perform action, you must not have the correct components selected!");
                }
            }

            //display tooltips
            if (GUI.tooltip == "cook")
            {
                string msg = "";
                Cbg.ActionButcher ab = new Cbg.ActionButcher(selectedDude,gm.InventoryView.GetSelectedItems());
                
                if (ab.ToButcher == null)
                    msg = "NO BUTCHERABLE ITEMS SELECTED";
                else
                {
                    msg = "Butchering " + ab.ToButcher.Name + " corpse\n\n";
                    msg += CbgMessageView.YieldData(ab.ButcherYields(gm.Data));
                }
                
                CbgGameManager.Inst.MessageView.Tooltip = msg;
            } else if(GUI.tooltip != "")
            {
                CbgGameManager.Inst.MessageView.Tooltip = GUI.tooltip;
            }

        }

        if (isCraftView)
        {
            int tooltipHeight = 20;
            int cstarty = starty + height + padding;
            int top = cstarty + tooltipHeight;
            int left = startx + padding;
            int numEntries = 0;
            int maxEntries = 4;

            foreach (var e in Cbg.Constants.newRecipes)
            {
                string tooltip = "";
                foreach (var f in e.output.items)
                    tooltip += tooltip == ""? f.ToString() : ","+f.ToString();
                tooltip += "\n" + e.input.ToString();



                string displayName;
                if(e.output.items.Count() == 1)
                    displayName = e.output.items[0].ToString();
                else
                    displayName = string.Join(",", e.output.items.Select(f=>f.ToString()).ToArray());

                ActionData dt = GetCraft(e, gm, selectedDude, selectedTile);
                if (dt.error != "")
                    displayName = "<color=red>" + displayName + "</color>";
                if(GUI.Button(new Rect(left,top,width,height),new GUIContent(displayName ,tooltip)))
                {
                    
                
                    if (dt.error != "")
                        gm.MessageView.AddMessage(dt.error);
                    else
                    {
                        if (dt.butcher != null)
                            gm.DoAction(dt.butcher);
                        else if (dt.craft != null)
                            gm.DoAction(dt.craft);
                        else
                            throw new Exception("unexpected");

                        isCraftView = false;
                    }
                }
                top += height + padding;
                numEntries++;
                if(numEntries >= maxEntries)
                {
                    top = cstarty + tooltipHeight;
                    numEntries = 0;
                    left += padding + width;
                }

            }
            //TODO temporary moved inventory tooltip back to the old spot
            //GUI.Label(new Rect(startx + padding, cstarty, totalWidth,tooltipHeight), GUI.tooltip);
            
            //if (showTooltip)
            {
                //CbgGameManager.Inst.MessageView.Tooltip = GUI.tooltip;
                GUIStyle style = new GUIStyle();
                style.normal.background = UnityUtilities.GetColorTexture(Color.black);
                style.normal.textColor = Color.white;
                var dim = style.CalcSize(new GUIContent(GUI.tooltip));
                GUI.Label(new Rect(Input.mousePosition.x, Screen.height-Input.mousePosition.y, dim.x,dim.y),GUI.tooltip,style);
            }
        }
    }

}

class MessageStack
{
    public string message;
    public int stack;

    public MessageStack(string message_, int stack_ = 1)
    {
        message = message_;
        stack = stack_;
    }
}

public class CbgMessageView : MonoBehaviour
{
    string mTooltip;
    public string Tooltip{ 
        get { return mTooltip; } 
        set { 
            if (value != "")
                mTooltip = value; 
        } 
    }
    List<MessageStack> mMessages = new List<MessageStack>();
    bool firstPass = true;
    void Start () {
        
    }
    void Update () {
        firstPass = true;
    }

    public void AddMessage(string aMessage)
    {
        if (aMessage == "")
            return;

        MessageStack ms = mMessages.LastOrDefault();

        if (ms != null && ms.message == aMessage)
            ++ms.stack;
        else
            mMessages.Add(new MessageStack(aMessage));
    }
    void OnGUI()
    {
        int padding = 5;
        int top = padding;
        int left = Cbg.Constants.mapWidth + Cbg.Constants.guiPadding + padding;
        int entryHeight = 20;
        int width = Cbg.Constants.messageBoxWidth - padding * 2;
        int maxMessages = (Cbg.Constants.topSectionHeight - padding * 2) / entryHeight;
        //int maxHeight = (Cbg.Constants.topSectionHeight - padding * 2);

        GUIStyle style = new GUIStyle(GUI.skin.label);
        style.alignment = TextAnchor.MiddleLeft;

        if (Tooltip == "")
        {
            foreach (var e in mMessages.Select((value, index) => new { value, index }).Skip(Mathf.Max(0,mMessages.Count - maxMessages)))
            {
                string msg = e.value.message;
                if (e.value.stack > 1)
                    msg += " (x" + e.value.stack + ")";

                GUI.Label(new Rect(left, top, width, entryHeight), new GUIContent(e.index.ToString("D5") + ": " + msg));
                top += entryHeight;
            }
        } else
        {
            GUI.Label(new Rect(left, top, width, Cbg.Constants.topSectionHeight - padding*2), Tooltip);
            if(!firstPass)
                mTooltip = "";  
        }
        firstPass = false;
    }

    static public string YieldData(Dictionary<Cbg.Item, Cbg.YieldAndBonus> yields)
    {
        Dictionary<string, double> y = new Dictionary<string, double>();

        foreach (var kv in yields)
            y.Add(kv.Key.Name, kv.Value.Total);

        return YieldData(y);
    }

    static public string YieldData(Dictionary<string, double> yields)
    {
        string ret = "";

        var arr = from kv in yields
                  orderby -kv.Value
                  select kv;

        List<string> some = new List<string>();
        List<string> few = new List<string>();

        foreach (var kv in arr)
        {
            double d = (int)(kv.Value * 100);
            d /= 100;

            if (d >= 1)
                ret += kv.Key + " " + d + "\n";
            else if (d > .1)
                some.Add(kv.Key);
            else
                few.Add(kv.Key);
        }

        if (some.Any())
            ret += "some " + string.Join(", ", some.ToArray()) + "\n";

        if (few.Any())
            ret += "few " + string.Join(", ", few.ToArray()) + "\n";

        return ret;
    }
}