﻿using UnityEngine;
using System.Collections.Generic;

public class CbgMainBehaviour : MonoBehaviour {
   
    public Texture2D[] textures;
    public CbgGameManager Game {get; private set;}

    //this classes sole responsibility is to create a new CbgGameManager
	void Start () {
        Game = gameObject.AddComponent<CbgGameManager>();

        textures = Resources.LoadAll<Texture2D>("");
        //foreach (var e in textures) Debug.Log(e);
	}



	void Update () {
	
	}


    //utilitiy
    static Dictionary<Color, Texture2D> mColorTextures = new Dictionary<Color, Texture2D>();
    public static Texture2D GetColorTexture(Color aColor)
    {
        if (!mColorTextures.ContainsKey(aColor))
        {
            int w = 2;
            int h = 2;
            Texture2D rgb_texture = new Texture2D(w, h);
            Color rgb_color = aColor;
            int i, j;
            for (i = 0; i < w; i++)
            {
                for (j = 0; j < h; j++)
                {
                    rgb_texture.SetPixel(i, j, rgb_color);
                }
            }
            rgb_texture.Apply();
            mColorTextures[aColor] = rgb_texture;
        }
        return mColorTextures[aColor];
    }
}
