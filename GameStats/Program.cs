﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Cbg;

namespace GameStats
{
    class Program
    {
        static void DudeStarveOut(StringBuilder output, double start, int food)
        {
            Dude d = Dude.GetTestDude();

            d.Satiation = 0.2 * start;

            int counter = 0;
            while (d.Satiation > 0)
            {
                d.FoodAdvance(food);
                counter++;
            }

            output.AppendFormat("dude from {0} to {1} (food {2}) in {3} steps\n", start, 0, food, counter);
        }
        
        static void DudeFeedOut(StringBuilder output, double start, double end, int food)
        {
            Dude d = Dude.GetTestDude();

            d.Satiation = 0.2 * start;

            int counter = 0;
            while (d.Satiation < 0.2 * end)
            {
                d.FoodAdvance(food);
                counter++;
            }

            output.AppendFormat("dude from {0} to {1} (food {2}) in {3} steps\n", start, (d.Satiation/.2).ToString("0.0"), food, counter);
        }
        static void DudeFreezeOut(StringBuilder output, double start, double heat)
        {
            Dude d = Dude.GetTestDude();

            d.Warmth = 0.2 * start;

            int counter = 0;
            while (d.Warmth > 0)
            {
                d.HeatAdvance(heat);
                counter++;
            }

            output.AppendFormat("dude from {0} to {1} (heat {2}) in {3} steps\n", start, 0, heat, counter);
        }
        
        static void DudeHeatOut(StringBuilder output, double start, double end, double heat)
        {
            Dude d = Dude.GetTestDude();

            d.Warmth = 0.2 * start;

            int counter = 0;
            while (d.Warmth < 0.2 * end)
            {
                d.HeatAdvance(heat);
                counter++;
            }

            output.AppendFormat("dude from {0} to {1} (heat {2}) in {3} steps\n", start, (d.Warmth/.2).ToString("0.0"), heat, counter);
        }

        static void SurvivalStats()
        {
            using (FileStream output = File.Open("SurvivalStats.txt", FileMode.Create))
            using (StreamWriter sw = new StreamWriter(output))
            {
                {
                    StringBuilder sbFood = new StringBuilder();
                    StringBuilder sbHeat = new StringBuilder();

                    for (int i = 1; i <= 3; ++i)
                    {
                        DudeFeedOut(sbFood, i, i + 1, i + 3);
                        DudeHeatOut(sbHeat, i, i + 1, i + 2);
                    }

                    DudeFeedOut(sbFood, 1, 4, 6);
                    DudeFeedOut(sbFood, 1, 4.5, 6);
                    DudeFeedOut(sbFood, 1, 4.9, 6);

                    DudeHeatOut(sbHeat, 1, 4, 5);
                    DudeHeatOut(sbHeat, 1, 4.5, 5);
                    DudeHeatOut(sbHeat, 1, 4.9, 5);

                    sw.Write(sbFood.ToString());
                    sw.WriteLine();
                    sw.Write(sbHeat.ToString());
                }

                {
                    StringBuilder sbFood = new StringBuilder();
                    StringBuilder sbHeat = new StringBuilder();

                    DudeStarveOut(sbFood, 5, 1);
                    DudeStarveOut(sbFood, 4, 1);
                    DudeStarveOut(sbFood, 1, 1);

                    DudeStarveOut(sbFood, 5, 0);
                    DudeStarveOut(sbFood, 4, 0);
                    DudeStarveOut(sbFood, 1, 0);

                    DudeFreezeOut(sbHeat, 5, 0);
                    DudeFreezeOut(sbHeat, 4, 0);
                    DudeFreezeOut(sbHeat, 1, 0);

                    sw.WriteLine();
                    sw.Write(sbFood.ToString());
                    sw.WriteLine();
                    sw.Write(sbHeat.ToString());
                }
            }

        }

        static void Main(string[] args)
        {
            SurvivalStats();
        }
    }
}
