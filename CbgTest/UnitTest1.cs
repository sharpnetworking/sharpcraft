﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Cbg;

namespace CbgTest
{
    //[TestClass]
    //public class UtilTest
    //{
    //    [TestMethod]
    //    public void Clamp()
    //    {
    //        Assert.AreEqual(Cbg.Tools.Clamp(75, -3.5, 4), 4);
    //        Assert.AreEqual(Cbg.Tools.Clamp(-75, -3.5, 4), -3.5);
    //        Assert.AreEqual(Cbg.Tools.Clamp(.7, 0, 1), .7);
    //    }
    //}
    
    [TestClass]
    public class FoodHeatTest
    {
        [TestMethod]
        public void FoodConsumption()
        {
            for (int i = 1; i <= 5; ++i)
            {
                double val = .2 * i;

                Dude d = Dude.GetTestDude();
                d.Satiation = val;
                d.FoodAdvance(1 + i);

                Assert.AreEqual(val, d.Satiation, .001, "survival on " + (i + 1));
            }
        }

        [TestMethod]
        public void HeatConsumption()
        {
            for (int i = 1; i <= 5; ++i)
            {
                double val = .2 * i;

                Dude d = Dude.GetTestDude();
                d.Warmth = val;
                d.HeatAdvance(i);

                Assert.AreEqual(val, d.Warmth, .001, "survival on " + i);
            }
        }

        public void TestDistribution(List<Dude> lst, List<int> expected, int food)
        {
            Inventory inv = new Inventory();

            for (int i = 0; i < food; ++i )
                inv.Add(new Item("edible"));

            var distr = ActionAdvanceDay.AssignFoodDistribution(inv, lst);

            Assert.AreEqual(lst.Count, expected.Count);
            Assert.AreEqual(distr.Count, expected.Count);
            
            for (int i = 0; i < lst.Count; ++i )
            {
                Assert.AreEqual(distr[lst[i]], expected[i]);
            }
        }

        [TestMethod]
        public void FoodDistribution()
        {
            {
                Dude d1 = Dude.GetTestDude();
                d1.Satiation = .1;

                Dude d2 = Dude.GetTestDude();
                d2.Satiation = .2;

                Dude d3 = Dude.GetTestDude();
                d3.Satiation = .3;

                List<Dude> lst = new List<Dude>() { d2, d3, d1 };
                List<int> exp = new List<int>() { 1, 0, 1 };

                TestDistribution(lst, exp, 2);
            }

            {
                Dude d1 = Dude.GetTestDude();
                d1.FoodPerDay = 2;

                Dude d2 = Dude.GetTestDude();
                d2.FoodPerDay = 3;

                Dude d3 = Dude.GetTestDude();
                d3.FoodPerDay = 4;

                Dude d4 = Dude.GetTestDude();
                d4.FoodPerDay = 5;

                List<Dude> lst = new List<Dude>() { d1, d2, d3, d4 };
                List<int> exp = new List<int>() { 2, 3, 4, 4 };

                TestDistribution(lst, exp, 2 + 3 + 4 + 4);
            }
        }

        public void TestFires(int fires, int logMax, int logs, double heat, int logsBurned, int firesLit)
        {
            Inventory inv = new Inventory();
            inv.Add(new Item("fireplace"), fires);
            inv.Add(new Item("firewood"), logs);

            double resultHeat;
            int resultBurned;
            int resultFiresLit;

            ActionAdvanceDay.HeatProduction(inv, logMax, out resultHeat, out resultBurned, out resultFiresLit);

            string msg = "fires " + fires + " logmax " + logMax + " logs " + logs;
            Assert.AreEqual(heat, resultHeat, .001, msg);
            Assert.AreEqual(logsBurned, resultBurned, msg);
            Assert.AreEqual(firesLit, resultFiresLit, msg);
        }

        [TestMethod]
        public void FireBurn()
        {
            double one = ActionAdvanceDay.ONE_LOG_HEAT;
            double two = one + ActionAdvanceDay.TWO_LOG_HEAT;
            //double three = two + ActionAdvanceDay.THREE_LOG_HEAT;

            TestFires(0, 5, 3, 0, 0, 0);
            TestFires(2, 10, 10, two * 2, 4, 2);
            TestFires(2, 3, 7, two + one, 3, 2);
            TestFires(2, 5, 3, two + one, 3, 2);
            TestFires(5, 6, 6, two + one * 4, 6, 5);
            TestFires(5, 3, 3, one * 3, 3, 3);
        }

        [TestMethod]
        public void DudeStats()
        {
            Dude d = Dude.GetTestDude();

            d.Warmth = .6;
            d.Satiation = .8;

            Assert.AreEqual(d.SatiationScore, 4d, .0001);
            Assert.AreEqual(d.WarmthScore, 3d, .0001);
            Assert.AreEqual(d.HealthScore, 3.5d, .0001);
            Assert.AreEqual(d.ExpAdjust, .125d, .0001);

            d.skills.AddExpExact("survival", 40);

            Assert.AreEqual(d.skills.GetLevel("survival"), 2);
            Assert.AreEqual(d.RestHP, 9.5d, .001);

            d.Warmth = .8;
            d.Satiation = .8;
            d.HP = .9;

            Assert.AreEqual(0, Formulas.DeathChance(d));

            d.Warmth = .2;
            d.Satiation = .2;
            d.HP = .75;

            Assert.AreEqual(.105, Formulas.DeathChance(d), .00001);
        }

    }
    [TestClass]
    public class CraftingTest
    {
        [TestMethod]
        public void EasyCraft()
        {
            var r = Cbg.Recipe.Create("firewood", "burnable_10");
            
            {
                List<ItemStack> l = new List<ItemStack>();

                l.Add(new ItemStack(new Item("burnable"), 10));

                var a = new ActionCraft(Dude.GetTestDude(), r, l.ToArray());

                Assert.IsTrue(a.TryCraft() != null);
            }

            {
                List<ItemStack> l = new List<ItemStack>();

                for (int i = 0; i < 10; ++i )
                    l.Add(new ItemStack(new Item(i.ToString(), "burnable"), 1));

                var a = new ActionCraft(Dude.GetTestDude(), r, l.ToArray());

                Assert.IsTrue(a.TryCraft() != null);
            }

            {
                List<ItemStack> l = new List<ItemStack>();

                l.Add(new ItemStack(new Item("burnable", "rock"), 10));
                l.Add(new ItemStack(new Item("fiber"), 10));

                var r1 = Cbg.Recipe.Create("firewood", "burnable_2, rock_3, fiber_5, burnable+rock_2");
                Assert.IsTrue(new ActionCraft(Dude.GetTestDude(), r1, l.ToArray()).TryCraft() != null);

                var r2 = Cbg.Recipe.Create("firewood", "burnable_5, rock_6, fiber_10");
                Assert.IsTrue(new ActionCraft(Dude.GetTestDude(), r2, l.ToArray()).TryCraft() == null);
            }
        }

        [TestMethod]
        public void FancyCraft()
        {

            List<ItemStack> l = new List<ItemStack>();

            l.Add(new ItemStack(new Item("burnable", "stick", "rock"), 3));
            l.Add(new ItemStack(new Item("burnable", "rock"), 2));
            l.Add(new ItemStack(new Item("burnable", "wood"), 4));

            var r1 = Cbg.Recipe.Create("firewood", "burnable_3, stick");
            var r2 = Cbg.Recipe.Create("firewood", "burnable_4, rock_2, stick_3");

            Assert.IsTrue(new ActionCraft(Dude.GetTestDude(), r1, l.ToArray()).TryCraft() != null);
            Assert.IsTrue(new ActionCraft(Dude.GetTestDude(), r2, l.ToArray()).TryCraft() != null);
        }
    }
}
