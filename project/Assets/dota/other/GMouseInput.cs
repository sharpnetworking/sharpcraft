﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class GInputMouseProfile
{
	public VectorSignal mPositions;
	public int mButton; //-1 means no button
	public GInputMouseProfile(int aButton = 0)
	{
		mButton = aButton;
		mPositions = new VectorSignal();//(to_relative(Input.mousePosition),Time.time);
	}
	
	public Vector3 get_last_mouse_position_relative() { return mPositions.get_last(); }
	public Vector3 get_last_mouse_position_absolute() { return to_absolute(mPositions.get_last()); }
	public Vector3 get_last_mouse_change_relative(){return mPositions.get_last_value_difference();}
	public Vector3 get_total_mouse_change_relative(){return mPositions.get_change();}

	//creates a new GInputMouseProfile that is offset based on the camera viewport rect
	public GInputMouseProfile create_camera_offset_input(Camera aCam)
	{
		GInputMouseProfile profile = new GInputMouseProfile(mButton);
		foreach(var e in mPositions.mValues)
		{
			Vector3 offsetValue = e.mValue;
			offsetValue.x -= aCam.rect.x;
			offsetValue.y -= aCam.rect.y;
			offsetValue.x /= aCam.rect.width;
			offsetValue.y /= aCam.rect.height;

			profile.mPositions.add_absolute(offsetValue,e.mTime);
		}
		return profile;
	}

	public static Vector3 to_absolute(Vector3 v)
	{
		Vector3 r = v;
		r.x *= Screen.width;
		r.y *= Screen.height;
		return r;
	}
	
	public static Vector3 to_relative(Vector3 v)
	{
		Vector3 r = v;
		r.x /= Screen.width;
		r.y /= Screen.height;
		return r;
	}
	
};


public class GInputMouseEventHandler
{
	public Func<GInputMouseProfile,bool> mMousePressed;
	public Action<GInputMouseProfile> mMouseMoved; //only happens when mouse is down
	public Action<GInputMouseProfile> mMouseUpMoved; //only happens when mouse is up
	public Action<GInputMouseProfile> mMouseReleased;
	public Action<float> mPinch;
	
	public bool mMouseDown = false;
	public float mTimeDown = 0;
	public GInputMouseEventHandler() { mMouseReleased += mouse_released; }
	void mouse_released(GInputMouseProfile mouse) { mMouseDown = false; }
	public void mouse_pressed() { mMouseDown = true; mTimeDown = Time.time; }
	public float time_down() { return Time.time - mTimeDown; }
	public bool was_mouse_pressed() { return mMouseDown; }
}

