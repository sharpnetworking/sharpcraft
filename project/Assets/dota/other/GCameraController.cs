﻿using UnityEngine;
using System.Collections.Generic;
public class GCameraController 
{
	GameObject mCameraParent = null;
	Camera mCamera = null;

	//position data
	float mZoom = 50; // lets say zoom is normalized to FOV of camera, so zoom of 5 means width of camera at mCenter is 5 units
    public Vector3 Center{ get; private set; }
	Vector3 mPan = Vector3.zero; //offset from object
	Bounds mPanningBounds;

	Quaternion mStartingRotation;
	float mTheta = 0;
	float mPhi = 0;
    public float Theta {
        get{ return mTheta;}
        set{ mTheta = value;
            update_rotation();
        }
    }
    public float Phi {
        get{ return mPhi;}
        set{ mPhi = value;
            update_rotation();}
    }

    public Quaternion Orientation{ get { return mCameraParent.transform.rotation; } }
	
	//useful properties
	public Vector3 CameraFocusPosition { get {return Center + Pan;} }
	public float DistanceFromCenter{ 
		get{return mZoom / Mathf.Tan(mCamera.fieldOfView);} 
		set{ mZoom = value*Mathf.Tan(mCamera.fieldOfView);}
	}
	public float ObjectSize { get { return mPanningBounds.extents.magnitude; } }

	public Vector3 Pan 
	{
		get { return mPan; }
		set
		{
			//mPan = mPanningBounds.clamp_point(mCenter + value) - mCenter;
			mPan = value;
			update_camera_position();
		}
	}

    public void ZoomToMouseLocation(Vector2 aMousePosition, float aChange)
    {
        //TODO
    }
	
	public float Zoom
	{
		get{return mZoom;}
		set
		{
			mZoom = Mathf.Clamp(value,0.1f,1000);
			update_camera_position();
		}
	}

    public bool Orthographic
    {
        get{ return mCamera.orthographic;}
        set{
            mCamera.orthographic = value;
            update_camera_position();
        }
    }

	//this will treat the current position of the camera as the neutral position
	public GCameraController(Camera aCamera, Vector3 aCenter) 
	{
		mCamera = aCamera;
		mCameraParent = new GameObject("genCameraParent");
		mCameraParent.transform.position = aCenter;
		mCamera.transform.parent = mCameraParent.transform;
		CenterOnPosition(aCenter,mCamera.transform.up);
	}

	public void CenterOnPosition(Vector3 aCenter, Vector3 aUp)
	{
		
		Center = aCenter;
		mCameraParent.transform.position = aCenter;
		mCamera.transform.LookAt(aCenter,aUp);
		
		mCamera.transform.parent = null;
		reset_rotation();
		mCameraParent.transform.rotation = mCamera.transform.rotation;
		mStartingRotation = mCameraParent.transform.rotation;
		mCamera.transform.parent = mCameraParent.transform;
		
		Zoom = Zoom;
		update_camera_position();
	}

	public void CenterOnPosition(Vector3 aCenter){
        CenterOnPosition(aCenter, mCamera.transform.up);
	}

	public void CenterOnObject(GameObject aObject, bool zoom = true)
	{
        Pan = Vector3.zero;
		MeshFilter mf = aObject.GetComponent<MeshFilter>();
		if (mf != null && mf.mesh != null)
		{ 
			float x1 = mf.mesh.bounds.extents.x;
			float x2 = mf.mesh.bounds.extents.y;
			float x3 = mf.mesh.bounds.extents.z;
			float[] vals = { x1, x2, x3 };
            mPanningBounds = new Bounds(aObject.transform.position, Vector3.one*Mathf.Max(vals)*aObject.transform.lossyScale.magnitude);
			if(zoom){
				mZoom = 1.0f*Mathf.Max(vals)*aObject.transform.lossyScale.magnitude;
				mZoom *= 0.6f; // zoom closer

			}
			CenterOnPosition(mf.mesh.bounds.center + mf.transform.position, aObject.transform.up);
		} else 
		{
			CenterOnPosition(aObject.transform.position,aObject.transform.up);
		}

	}
	
	void update_camera_position()
	{

        if (!Orthographic)
        {
            float d = DistanceFromCenter;
            mCamera.transform.position = CameraFocusPosition - mCamera.transform.forward * d;
        }
        else
        {
            mCamera.orthographicSize = mZoom;
            mCamera.transform.position = CameraFocusPosition - mCamera.transform.forward * 100;
        }

		mCamera.transform.LookAt(CameraFocusPosition,mCamera.transform.up);

	}

	
	public void pan_relative(Vector3 aPan)
	{
		Pan = Pan + aPan;
	}
	
	public void zoom_relative(float aDeltaDistance)
	{
		Zoom = Mathf.Clamp(Zoom + aDeltaDistance,1f,Mathf.Infinity); //should probably put some more reasonable limits on zoom
	}

	public void reset_rotation()
	{
		mTheta = 0;
		mPhi = 0;
		update_rotation();
	}

	public void rotate(float aX, float aY)
	{
		
		float xClampVal = 9999999;
		float yClampVal = 85;
		mTheta = Mathf.Clamp(mTheta+aX,-xClampVal,xClampVal);
		mPhi = Mathf.Clamp(mPhi+aY,-yClampVal,yClampVal);	
		update_rotation();
	}

	void update_rotation()
	{
		mCameraParent.transform.rotation = mStartingRotation;
		//Quaternion r1 = Quaternion.AngleAxis(mTheta, mCameraParent.transform.up);
		//Quaternion r2 = Quaternion.AngleAxis(mPhi, mCameraParent.transform.right);
		//mCameraParent.transform.rotation *= r2 * r1;
		mCameraParent.transform.rotation *= Quaternion.Euler(-mPhi, mTheta, 0); //
	}

    //you must call this after calling rotate if you want the changes to persist
    public void set_rotation_up(Vector3 aUpVector)
    {
        mCamera.transform.LookAt(CameraFocusPosition, aUpVector);
    }

	public Texture2D take_screenshot()
	{
		var cam = mCamera;
		
		int resWidth = Screen.width; //800;
		int resHeight = Screen.height; //450;
		RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
		cam.targetTexture = rt;
		RenderTexture.active = rt;
		Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
		CameraClearFlags ccf = cam.clearFlags;
		cam.clearFlags = CameraClearFlags.SolidColor;
		//cam.backgroundColor = new Color(0, 0, 0, 1);
		//cam.DoClear(); obsolete??
		cam.clearFlags = ccf;
		cam.Render();
		screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		screenShot.Apply();
		
		cam.targetTexture = null;
		RenderTexture.active = null; // JC: added to avoid errors
		GameObject.Destroy(rt);
		
		return screenShot;
	}
	
}
