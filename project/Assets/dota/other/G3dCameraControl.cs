using UnityEngine;
using System.Collections;

//this class actually creates the camera and hooks up GInput to operate it
public class G3dCameraControl 
{
	public Camera mCamera = null;
	public GInputMouseEventHandler mCameraMouseHandler = new GInputMouseEventHandler();
	public GCameraController mCameraController;

    public float ZoomScaling { get; set; }
    public float RotationScaling {get; set;}
	public float PanScaling { get; set; }

    public System.Action OnScreenChange { get; set; }

    public Quaternion BaseOrientation {get; set;}

	public bool EnableRotation{ get; set; }

    GCameraSnap CameraAnimationTarget{ get; set; }
    public bool IsAnimating{ get { return CameraAnimationTarget != null; } }


	       
    Vector2 mLastScreenSize;
	public G3dCameraControl()
	{
        ZoomScaling = 1;
        RotationScaling = 1;
        PanScaling = 1;
        mLastScreenSize = new Vector2(Screen.width, Screen.height);
		EnableRotation = true;

		initialize();
	}

	public void initialize()
	{
		mCameraMouseHandler.mMousePressed += this.camera_handle_press;
		mCameraMouseHandler.mMouseMoved += this.camera_handle_mouse_down_motion;
		mCameraMouseHandler.mMouseReleased += this.camera_handle_release;
		mCameraMouseHandler.mPinch += this.camera_handle_pinch;
		
		mCamera = (new GameObject("genThreeViewCamera")).AddComponent<Camera>();
		mCamera.backgroundColor = new Color(1f,1f,1f);
        mCamera.farClipPlane = 5000;
		mCamera.transform.position = new Vector3(0,0,-10); 
		mCamera.transform.LookAt(Vector3.zero);
		mCameraController = new GCameraController(mCamera,Vector3.zero);
		mCameraController.Zoom = 10;


		//TODO need to center on object to set mPanningbounds in camera
	}

    public void Update()
    {
        if (mLastScreenSize != new Vector2(Screen.width, Screen.height))
        {
            mLastScreenSize = new Vector2(Screen.width, Screen.height);
            if(OnScreenChange != null)
                OnScreenChange();
        }

        if (IsAnimating)
        {
            GCameraSnap current = new GCameraSnap(mCameraController);
            GCameraSnap interp = new GCameraSnap(mCameraController);
            interp.pan = Vector3.MoveTowards(current.pan, CameraAnimationTarget.pan, 700 * PanScaling * Time.deltaTime);
            interp.zoom = Mathf.MoveTowards(current.zoom, CameraAnimationTarget.zoom, 300 * ZoomScaling * Time.deltaTime);
            interp.phi = Mathf.MoveTowards(current.phi, CameraAnimationTarget.phi, 500 * RotationScaling * Time.deltaTime);
            interp.theta = Mathf.MoveTowards(current.theta, CameraAnimationTarget.theta, 500 * RotationScaling * Time.deltaTime);
            interp.SetGCamera(mCameraController);

            if (Mathf.Abs(interp.phi - CameraAnimationTarget.phi) < 0.001f && 
                Mathf.Abs(interp.theta - CameraAnimationTarget.theta) < 0.001f &&
                Mathf.Abs(interp.zoom - CameraAnimationTarget.zoom) < 0.001f &&
                (interp.pan - CameraAnimationTarget.pan).sqrMagnitude < 0.00001)
            {
                CameraAnimationTarget.SetGCamera(mCameraController);
                CameraAnimationTarget = null;
            }
        }

    }

    //only call me if you want keyboard controls
    //returns true if something moved
    public bool camera_keyboard_update(float speed = 30)
    {
        Vector2 pan = Vector2.zero;
        if (Input.GetKey(KeyCode.LeftArrow))
            pan.x = 1;
        else if (Input.GetKey(KeyCode.RightArrow))
            pan.x = -1;
        if (Input.GetKey(KeyCode.UpArrow))
            pan.y = 1;
        else if (Input.GetKey(KeyCode.DownArrow))
            pan.y = -1;
        if (pan != Vector2.zero)
        {
            pan_camera(pan * speed * Time.deltaTime);
            return true;
        }
        return false;
    }

    public void zoom_pan_camera(Vector3 aLocation, float change)
    {
        if (change != 0)
        {
            float lz = mCameraController.Zoom;
            mCameraController.zoom_relative(-change * ZoomScaling * mCameraController.ObjectSize / 200.0f);
            float rz = 1 - mCameraController.Zoom/lz;
            mCameraController.pan_relative((aLocation - mCameraController.Pan)*rz);
        }
    }

	public void camera_handle_pinch(float change)
	{
		//TODO switch to object relative version
		if(change != 0)
			mCameraController.zoom_relative(-change*ZoomScaling*mCameraController.ObjectSize/200.0f);

			//mCameraController.zoom_relative(-change/10.0f);
	}
	public bool camera_handle_press(GInputMouseProfile mouse)
	{ 
		return true;
	}
	
    public void pan_camera(Vector3 aPan, bool aCameraRelative = false, bool aPanCenter = false)
	{
        //TODO this should be made proportional to zoom
        //TODO this is broken!
        var orientation = BaseOrientation;
        if (aCameraRelative)
            orientation = orientation  *  mCameraController.Orientation ;
        Vector3 pan = orientation*(Vector3.up * aPan.y + Vector3.right * aPan.x + Vector3.forward*aPan.z)*5*PanScaling;
        if (!aPanCenter)
            mCameraController.pan_relative (pan);
        else
            mCameraController.CenterOnPosition(mCameraController.CameraFocusPosition + pan);
	}
	
	
	public void camera_handle_mouse_down_motion(GInputMouseProfile mouse)
	{
		Vector3 normalizedMouseChange = mouse.get_last_mouse_change_relative();
		normalizedMouseChange.x /= (float)Screen.width;
		normalizedMouseChange.y /= (float)Screen.height;
		int mode = 1;
		if(mouse.mButton == 2 || Input.GetKey(KeyCode.LeftAlt) || !EnableRotation) //middle mouse button // or when rotation is disabled
			mode = 0;
		if(mode == 0)
		{
			Vector2 pan = new Vector2(normalizedMouseChange.x, -normalizedMouseChange.y);
			pan_camera(pan * 5000);
		}
		else if (mode == 1)
		{
			mCameraController.rotate(normalizedMouseChange.x*50000*RotationScaling, normalizedMouseChange.y*30000*RotationScaling);
            mCameraController.set_rotation_up(BaseOrientation*Vector3.up);
		}
	}

	public void camera_handle_release(GInputMouseProfile mouse){}

	public void set_camera_viewport(Rect targetRect)
	{
		mCamera.rect = targetRect;
	}

    public void animate_camera(GCameraSnap aTarget)
    {
        CameraAnimationTarget = aTarget;
    }

    public class GCameraSnap
    {
        public Vector3 pan;
        public float phi, theta, zoom;
        public GCameraSnap(GCameraController aCam)
        {
            pan = aCam.Pan;
            phi = aCam.Phi;
            theta = aCam.Theta;
            zoom = aCam.Zoom;
        }
        public void SetGCamera(GCameraController aCam)
        {
            aCam.Pan = pan;
            aCam.Theta = theta;
            aCam.Phi = phi;
            aCam.Zoom = zoom;
        }
    }
}
