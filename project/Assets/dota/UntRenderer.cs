﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

using Dota;
public class UntRenderChunk
{
    public Box WorldBox {get; private set;}
    GameObject[,,] Blocks {get; set;}
    public static UntRenderChunk Create(RenderChunk aData)
    {
        UntRenderChunk r = new UntRenderChunk();
        r.ReCreate(aData);
        return r;
    }

    public void ReCreate(RenderChunk aData)
    {

        WorldBox = aData.WorldBox;
        var size = WorldBox.size;
        Blocks = new GameObject[size.x,size.y,size.z];
        int numBlocks = 0;
        foreach (var e in aData.WorldBox.Range())
        {
            var blockIndex = e-WorldBox.corner;
            bool hasBlock = aData[e] != 0;

            if(hasBlock)
                numBlocks++;


            bool isSurrounded = 
                (e.x >= size.x-1 || aData[new Point(e.x + 1,e.y,e.z)] != 0) &&
                (e.x <= 0 || aData[new Point(e.x - 1,e.y,e.z)] != 0) &&
                (e.y >= size.y-1 || aData[new Point(e.x,e.y + 1,e.z)] != 0) &&
                (e.y <= 0 || aData[new Point(e.x,e.y - 1,e.z)] != 0) &&
                (e.z >= size.z-1 || aData[new Point(e.x,e.y,e.z + 1)] != 0) &&
                (e.z <= 0 || aData[new Point(e.x,e.y,e.z - 1)] != 0);


            //bool isSurrounded = false;
            if(hasBlock && !isSurrounded)
            {
                var position = e;
                var parent = new GameObject(position.ToString());
                var child = GameObject.CreatePrimitive(PrimitiveType.Cube);
                child.transform.parent = parent.transform;
                parent.transform.position = new Vector3(position.x,position.y,position.z);
                Blocks[blockIndex.x,blockIndex.y,blockIndex.z] = parent;
            } 
            else
                DestroyBlock(blockIndex);
        }
        Debug.Log(numBlocks);
    }

    public void Destroy()
    {
        foreach (var e in Blocks)
            GameObject.Destroy(e);
    }

    void DestroyBlock(Point aPoint)
    {
        if (Blocks [aPoint.x, aPoint.y, aPoint.z] != null)
        {
            GameObject.Destroy(Blocks [aPoint.x, aPoint.y, aPoint.z]);
            Blocks [aPoint.x, aPoint.y, aPoint.z] = null;
        }
    }

    //TODO function for updating after an operation, ideally not totally inefficient
}

public class UntWorld
{
    public List<UntRenderChunk> RenderChunks{get; private set;}
    public Point Center{get; set;}

    public UntWorld()
    {
        RenderChunks = new List<UntRenderChunk>();
        Center = new Point(0,0,0);
    }
    

    public void UpdateFromWorldData(WorldData aData)
    {
        Point hsize = new Point(100,100,100);
        Box rbox = new Box(Center - hsize, hsize*2);
        //Debug.Log(rbox);
        var mts = aData.GetMegaTilesIntersectingBox(rbox);
        foreach (var e in RenderChunks.ToArray())
        {
            var mtmatch = mts.Where(f=>f.WorldBox == e.WorldBox);
            if( mtmatch.Count() == 1)
            {
                //TODO this is stupid slow because I keep recreating render chunks... ugg, just get rid of render chunks???
                //or maybe have render chunks take MegaTiles as references instead of keeping a copy of the data themselves ya?
                e.ReCreate(new RenderChunk(mtmatch.First().WorldBox,aData.Grid));
                //remove processed from mts
                mts.Remove(mtmatch.First());
            }
            else
            {
                //clear out RenderChunks that are no longer clase
                //TODO this should not clear out unless the render chunk is more than 2 MegaTiles away (say)
                e.Destroy();
                RenderChunks.Remove(e);
            }
        }
        foreach (var e in mts)
        {
            RenderChunks.Add(UntRenderChunk.Create(new RenderChunk(e.WorldBox,aData.Grid)));
        }
    }

}

public class UntRenderer : MonoBehaviour {

    G3dCameraControl mCamera;
    GameObject mFocusObject;

    UntWorld mWorld;
    WorldData mData;

    public class BlockFace
    {
        public WorldPoint Block {get; set;}
        public Dota.CubeDirection Face {get; set;}
    }
    BlockFace RaycastBlock(Camera aCam, Vector3 aViewportPos)
    {
        var ray = aCam.ViewportPointToRay(aViewportPos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) //TODO do layermask
        {
            var r = new BlockFace(); //TODO
            r.Block = new WorldPoint(Point.Parse(hit.collider.transform.parent.name)); //TODO parse hit.collider.transform.parent.name
            if(hit.normal.x > 0)
                r.Face = CubeDirection.RIGHT;
            else if(hit.normal.x < 0)
                r.Face = CubeDirection.LEFT;
            else if(hit.normal.y > 0)
                r.Face = CubeDirection.UP;
            else if(hit.normal.y < 0)
                r.Face = CubeDirection.DOWN;
            else if(hit.normal.z > 0)
                r.Face = CubeDirection.FRONT;
            else if(hit.normal.z < 0)
                r.Face = CubeDirection.BACK;
            return r;
        }
        return null;
    }

    void Start () {

        //initialize the camera
        mFocusObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        mFocusObject.transform.localScale = Vector3.one * 10;
        GameObject.Destroy(mFocusObject.GetComponent<Renderer>());
        mCamera = new G3dCameraControl();
        mCamera.mCameraController.CenterOnObject(mFocusObject);
        mCamera.RotationScaling = 10;
        var light = mCamera.mCamera.GetComponent<Camera>().gameObject.AddComponent<Light>();
        light.range = 10;
        //light.type = LightType.Directional;

        //TODO load in some arbitrary set of data
        mWorld = new UntWorld();
        mData = WorldData.TestWorld1();
        mWorld.UpdateFromWorldData(mData);
    }



    void Update()
    {
        handle_mouse_input();
        Vector3 pan = Vector3.zero;
        if (Input.GetKey(KeyCode.LeftArrow))
            pan.x = -1;
        else if (Input.GetKey(KeyCode.RightArrow))
            pan.x = 1;
        if (Input.GetKey(KeyCode.UpArrow))
            pan.z = 1;
        else if (Input.GetKey(KeyCode.DownArrow))
            pan.z = -1;
        mCamera.pan_camera(pan,true,true);
        if (pan != Vector3.zero)
        {
            //mCamera.mCameraController.CenterOnPosition(mCamera.mCameraController.CameraFocusPosition + pan);
        }
    }



    //mouse stuff
    GInputMouseProfile mMouse = new GInputMouseProfile();
    void handle_mouse_input()
    {
        //up mouse stuff
        GInputMouseProfile upMouse = new GInputMouseProfile(-1);
        upMouse.mPositions.add_absolute(GInputMouseProfile.to_relative(Input.mousePosition), Time.time);
        //up mouse handlers go here



        //pinch stuff
        handle_pinch(mCamera.mCameraMouseHandler);
        
        //down mouse stuff
        for (int i = 0; i < 1; i++)
        {
            if (Input.GetMouseButtonDown(i))
                mMouse = new GInputMouseProfile(i);
            if (Input.GetMouseButton(i))
                mMouse.mPositions.add_absolute(GInputMouseProfile.to_relative(Input.mousePosition), Time.time);

            //TODO move this into its own mouseHandler maybe???

            if (Input.GetMouseButtonDown(i)){
                var block = RaycastBlock(mCamera.mCamera.GetComponent<Camera>(),GInputMouseProfile.to_relative(Input.mousePosition));
                if(block != null)
                {
                    Debug.Log(block.Block);
                }
            }

            handle_mouse(mCamera.mCameraMouseHandler);
        }
    }
    public void handle_pinch(GInputMouseEventHandler mHandler)
    {
        float zoom = Input.GetAxis("Mouse ScrollWheel")*50; //tmporarily multiplying a factor to do quicker zoom
        //limit zoom based on time
        float maxMotion = Time.deltaTime*10;
        zoom = Mathf.Clamp(zoom,-maxMotion,maxMotion);
        mHandler.mPinch(zoom);

        //TODO location dependent zooming eventually
        //var location = m3d.ProductPlane.raycast_plane(mCamera.mCamera.ScreenToViewportPoint(Input.mousePosition), mCamera.mCamera);
        //mCamera.zoom_pan_camera(location,zoom);
    }
    public bool handle_mouse(GInputMouseEventHandler mHandler)
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (mHandler.mMousePressed(mMouse))
            {
                mHandler.mouse_pressed();
                return true;
            }
            return false;
        }
        if (!mHandler.was_mouse_pressed()) 
            return false;
        if(mHandler.mMouseMoved != null)
            mHandler.mMouseMoved(mMouse);
        if (Input.GetMouseButtonUp(0))
            if(mHandler.mMouseReleased != null)
                mHandler.mMouseReleased(mMouse);
        
        return true;
    }
}
