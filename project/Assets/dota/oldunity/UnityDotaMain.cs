﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Dota;

public class UnityDotaMain : MonoBehaviour {

    uint levels = 15;
    int dim = 32768;
    int halfDim = 32768/2;
    
    const int subDim = 40;

    int xMult = subDim;
    int zMult = subDim;
    int yMult = subDim / 2;

    PointPoTOctTree<int> mTree;
    //Dictionary<Point,int> mTree2;
    GameObject mFocusObject;
    G3dCameraControl mCamera;
    Dictionary<Point,GameObject> mBlocks = new Dictionary<Point, GameObject>();
    //PointPoTOctTree<GameObject> mBlocks2;

    SimplexNoise.Noise mNoise;

	void Start () {
        mFocusObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        mFocusObject.transform.localScale = Vector3.one * 10;
        GameObject.Destroy(mFocusObject.GetComponent<Renderer>());

        mCamera = new G3dCameraControl();
        mCamera.mCameraController.CenterOnObject(mFocusObject);
        mCamera.RotationScaling = 10;
        var light = mCamera.mCamera.GetComponent<Camera>().gameObject.AddComponent<Light>();
        light.range = 100;
        //light.type = LightType.Directional;

        mNoise = new SimplexNoise.Noise("aoeue");
        (new System.Random()).NextBytes(mNoise.perm); 

        mTree = new PointPoTOctTree<int>(levels);
        mTree.Offset = Point.one * -halfDim;
        //mTree2 = new Dictionary<Point, int>();

        GenerateTiles(0);
        StartCoroutine(Sup());

    }

    System.Collections.IEnumerator Sup()
    {
        for (int counter = 0; ; ++counter )
        {

            GenerateTiles(counter);

            yield return new WaitForSeconds(2);
        }
    }

    void GenerateTiles(int counter)
    {
        float t1 = Time.realtimeSinceStartup;
        //mTree2.Clear();
        float scaling = 30f;
        foreach (var e in Point.Range(new Point(xMult, yMult, zMult)))
        {
            if (mNoise.Generate((e.x + counter) / scaling, e.y / scaling, e.z / scaling) + 2 * (.5f - ((e.y) / (float)yMult)) > 0f)
            {
                mTree.SetValue(e - Point.one * (subDim / 2), 1);
                //mTree2[e - Point.one * (subDim / 2)] = 1;

            }
            else
            {
                mTree.SetValue(e - Point.one * (subDim / 2), 0);
                //mTree2[e - Point.one * (subDim / 2)] = 0;
            }
        }

        float t2 = Time.realtimeSinceStartup;
        foreach (var e in mBlocks)
            GameObject.Destroy(e.Value);

        mBlocks = new Dictionary<Point, GameObject>();

        foreach (var e in mTree.NonEmpty())
        //foreach (var note in mTree2)
        {
            mBlocks[e.position] = null;
        }
        float t3 = Time.realtimeSinceStartup;

        foreach (var e in mBlocks.Keys.ToArray())
        {
            //if(Point.SymmetricRange(Point.one).Where(f=>f.x*f.y == 0 && f.y*f.z == 0 && f.z*f.x == 0).Where(f=>mTree2.ContainsKey(f+e.position)).Count() != 7)
            if(Point.SymmetricRange(Point.one).Where(f=>f.x*f.y == 0 && f.y*f.z == 0 && f.z*f.x == 0).Where(f=>mBlocks.ContainsKey(f+e)).Count() != 7)
            //if(Point.SymmetricRange(Point.one).Where(f=>f.x*f.y == 0 && f.y*f.z == 0 && f.z*f.x == 0).Where(f=>mTree.GetValue(f+e) != 0).Count() != 7)
            {
                
                mBlocks[e] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                mBlocks[e].transform.position = new Vector3(e.x,e.y,e.z);
            }
        }
        float t4 = Time.realtimeSinceStartup;

        string output = "";
        for (int i = 1; i < 4; i++)
            output += " " + ((new float[]{t1,t2,t3,t4})[i] - (new float[]{t1,t2,t3,t4})[i-1]);
    }
	
	void Update () {
        handle_mouse_input();
	}

    GInputMouseProfile mMouse = new GInputMouseProfile();
    void handle_mouse_input()
    {
        //up mouse stuff
        GInputMouseProfile upMouse = new GInputMouseProfile(-1);
        upMouse.mPositions.add_absolute(GInputMouseProfile.to_relative(Input.mousePosition), Time.time);
        //up mouse handlers go here
        
        //pinch stuff
        handle_pinch(mCamera.mCameraMouseHandler);
        
        
        //down mouse stuff
        for (int i = 0; i < 1; i++)
        {
            if (Input.GetMouseButtonDown(i))
                mMouse = new GInputMouseProfile(i);
            if (Input.GetMouseButton(i))
                mMouse.mPositions.add_absolute(GInputMouseProfile.to_relative(Input.mousePosition), Time.time);
            handle_mouse(mCamera.mCameraMouseHandler);
        }
    }
    public void handle_pinch(GInputMouseEventHandler mHandler)
    {
        float zoom = Input.GetAxis("Mouse ScrollWheel")*50; //tmporarily multiplying a factor to do quicker zoom
        //limit zoom based on time
        float maxMotion = Time.deltaTime*1000;
        zoom = Mathf.Clamp(zoom,-maxMotion,maxMotion);
        mHandler.mPinch(zoom);
    }
    public bool handle_mouse(GInputMouseEventHandler mHandler)
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (mHandler.mMousePressed(mMouse))
            {
                mHandler.mouse_pressed();
                return true;
            }
            return false;
        }
        if (!mHandler.was_mouse_pressed()) 
            return false;
        if(mHandler.mMouseMoved != null)
            mHandler.mMouseMoved(mMouse);
        if (Input.GetMouseButtonUp(0))
            if(mHandler.mMouseReleased != null)
                mHandler.mMouseReleased(mMouse);
        
        return true;
    }

}