﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Dota;
public class DotaRenderChunk
{

    public class RenderPair
    {
        public GameObject go;
        public byte id;
    }


    Dictionary<Point,RenderPair> mRenderBlocks; //point is in absolute space

    Box mBounds; //in absolute space
    byte[,,] mBlocks;


    Point mRenderOffset;
    public Point RenderOffset //converts absolute space to render space (translation only obv)
    {
        get{
            return mRenderOffset;
        }
        set{
            Point change = value - RenderOffset;
            RenderOffset = value;
            if(mRenderBlocks != null)
                foreach (var e in mRenderBlocks.Values)
                    e.go.transform.position = e.go.transform.position + new Vector3(change.x,change.y,change.z);
        }
    }


    public DotaRenderChunk(Box aBounds, Point aRenderOffset)
    {
        mBounds = aBounds;
        RenderOffset = aRenderOffset;
    }


    public bool IsGenerating{get; private set;}
    public void FinishGenerating()
    {
        //TODO
        throw new UnityException("not implemented");
    }

    public IEnumerator Generate(WorldData aData)
    {
        IsGenerating = true;

        //TODO set the data in mBlocks
        //find its boundary (can be slow??)
            //should this query neighboring chunks??
        //create render objects

        IsGenerating = false;
        yield break;
    }

    public void Destroy()
    {
        foreach (var e in mRenderBlocks)
            GameObject.Destroy(e.Value.go);
        mRenderBlocks.Clear();
    }

    //this is faster than regenerating
    public void SetBlock(Point aPos, byte aType)
    {
        //set the raw block
        var rawPt = aPos - mBounds.LDBCorner;
        bool aRepair = false;
        if (mBlocks [rawPt.x, rawPt.y, rawPt.z] == aType)
            aRepair = true;
        mBlocks [rawPt.x, rawPt.y, rawPt.z] = aType;

        RenderPair dm;
        if (!mRenderBlocks.TryGetValue(aPos, out dm))
            dm = null;
        //set the render block
        if (aType == 0)
        {
            if(!aRepair) //we never need to do this if we are in repair 
            {
                if(dm != null)
                    GameObject.Destroy(dm.go);
                mRenderBlocks.Remove(aPos);
            }
        } else
        {
            bool needCreate = false;
            foreach (var e in Point.StraightNeighbors(rawPt))
                if(mBlocks[e.x,e.y,e.z] == 0)
                    needCreate = true;
            if(needCreate)
            {
                if(dm == null)
                    mRenderBlocks[aPos] = dm = new RenderPair();
                if(dm.id != aType)
                {
                    dm.id = aType;
                    if(dm.go == null)
                        dm.go = GameObject.CreatePrimitive(PrimitiveType.Cube); 
                    var nPos = RenderOffset + aPos;
                    dm.go.transform.position = nPos.ToUnityVector3();
                    mBlocks[rawPt.x,rawPt.y,rawPt.z] = aType;
                }
            }
        }

        //fix all the holes
        if (!aRepair)
        {
            foreach (var e in Point.StraightNeighbors(rawPt))
            {
                SetBlock(e + mBounds.LDBCorner, mBlocks[e.x,e.y,e.z]);
            }
        }
    }

}
