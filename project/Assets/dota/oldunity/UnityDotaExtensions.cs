﻿using UnityEngine;
using System.Collections;
using Dota;
public static class UnityDotaExtensions 
{
    public static Vector3 ToUnityVector3(this Point aPoint)
    {
        return new Vector3(aPoint.x,aPoint.y,aPoint.z);
    }
}
