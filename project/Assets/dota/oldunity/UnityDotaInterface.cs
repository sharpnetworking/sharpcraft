﻿using UnityEngine;
using System.Collections;
using Dota;

public class UnityDotaInterface  : MonoBehaviour
{

    GameObject mFocusObject;
    G3dCameraControl mCamera;

    public void Update()
    {
        handle_mouse_input();
        handle_keyboard_input();
    }

    
    public bool RaycastMouse(Camera aCamera, ref Point outPos, ref CubeDirection outDir)
    {
        RaycastHit ray;
        if (Physics.Raycast(aCamera.ScreenPointToRay(Input.mousePosition), out ray))
        {
            Debug.Log("hit " + ray.point + " " + ray.normal);

            //TODO shift by offset
            //outPos = RenderVector3ToDotaPoint(ray.collider.gameObject.transform.position);
            outPos = Point.zero;

            if(ray.normal.x < 0) 
                outDir = CubeDirection.LEFT;
            if(ray.normal.x > 0) 
                outDir = CubeDirection.RIGHT;
            if(ray.normal.y < 0) 
                outDir = CubeDirection.DOWN;
            if(ray.normal.y > 0) 
                outDir = CubeDirection.UP;
            if(ray.normal.z < 0) 
                outDir = CubeDirection.BACK;
            if(ray.normal.z > 0) 
                outDir = CubeDirection.FRONT;
            return true;
        }
        return false;
    }


    void handle_keyboard_input()
    {
        //TODO block pan focus object relative to terrain
        Vector2 pan = Vector2.zero;
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            pan.x = -1;
        if (Input.GetKeyDown(KeyCode.RightArrow))
            pan.x = 1;
        if (Input.GetKeyDown(KeyCode.DownArrow))
            pan.x = -1;
        if (Input.GetKeyDown(KeyCode.UpArrow))
            pan.x = 1;
    }


    
    GInputMouseProfile mMouse = new GInputMouseProfile();
    void handle_mouse_input()
    {
        //up mouse stuff
        GInputMouseProfile upMouse = new GInputMouseProfile(-1);
        upMouse.mPositions.add_absolute(GInputMouseProfile.to_relative(Input.mousePosition), Time.time);
        //up mouse handlers go here
        
        //pinch stuff
        handle_pinch(mCamera.mCameraMouseHandler);
        
        
        //down mouse stuff
        for (int i = 0; i < 1; i++)
        {
            if (Input.GetMouseButtonDown(i))
                mMouse = new GInputMouseProfile(i);
            if (Input.GetMouseButton(i))
                mMouse.mPositions.add_absolute(GInputMouseProfile.to_relative(Input.mousePosition), Time.time);

            //TOOD raycast

            handle_mouse(mCamera.mCameraMouseHandler);
        }
    }
    public void handle_pinch(GInputMouseEventHandler mHandler)
    {
        float zoom = Input.GetAxis("Mouse ScrollWheel")*50; //tmporarily multiplying a factor to do quicker zoom
        //limit zoom based on time
        float maxMotion = Time.deltaTime*1000;
        zoom = Mathf.Clamp(zoom,-maxMotion,maxMotion);
        mHandler.mPinch(zoom);
    }
    public bool handle_mouse(GInputMouseEventHandler mHandler)
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (mHandler.mMousePressed(mMouse))
            {
                mHandler.mouse_pressed();
                return true;
            }
            return false;
        }
        if (!mHandler.was_mouse_pressed()) 
            return false;
        if(mHandler.mMouseMoved != null)
            mHandler.mMouseMoved(mMouse);
        if (Input.GetMouseButtonUp(0))
            if(mHandler.mMouseReleased != null)
                mHandler.mMouseReleased(mMouse);
        
        return true;
    }   
}
