﻿using UnityEngine;
using System.Collections.Generic;
using Dota;
using System;

namespace Dota{
    [Serializable]
    public class WorldData 
    {
        //offset is (0,0,0) where (0,0,0) is bottom left back corner of megatile
        public MegaTileGrid Grid{ get; private set; }

        //--------------
        //ctors
        //--------------
        public WorldData(Point aTilingSize)
        {
            Grid = new MegaTileGrid(aTilingSize);
        }

        //will build world from aMegaTileSize sized MegaTiles from bottom left back corner of aBox until they do not fit (wont go over)
        public static WorldData EmptyWorld(Box aBox, Point aTilingSize)
        {
            WorldData r = new WorldData(aTilingSize);
            for (int i = aBox.LDBCorner.x; i+aTilingSize.x <= aBox.RUFCorner.x; i += aTilingSize.x)
                for (int j = aBox.LDBCorner.y; j+aTilingSize.y <= aBox.RUFCorner.y; j += aTilingSize.y)
                    for (int k = aBox.LDBCorner.z; k+aTilingSize.z <= aBox.RUFCorner.z; k += aTilingSize.z)
                    {
                        var c = new Point(i,j,k);
                        r.Grid.Data.Add(new WorldPoint(c),new MegaTile(new WorldPoint(c),aTilingSize));
                    }
            return r;
        }

        public static WorldData TestWorld1()
        {
            WorldData r = EmptyWorld(new Box(new Point(-128, -128, -128), new Point(256, 256, 256)), new Point(32,32,32));

            foreach (var e in Point.SymmetricRange(new Point(16,16,16)))
            //foreach (var e in Point.Range(new Point(16,16,16)))
            {
                //Debug.Log(e + " " + r.GetMegaTile(new WorldPoint(e)).Corner);
                var scaling = 10f;
                var counter = 1;
                var yMult = 10;
                if (new SimplexNoise.Noise("aoeu").Generate((e.x + counter) / scaling, e.y / scaling, e.z / scaling) + 2 * (.5f - ((e.y) / (float)yMult)) > 0f)
                //if(UnityEngine.Random.value < 0.1f)
                {
                    r.GetMegaTile(new WorldPoint(e)).SetValue(new WorldPoint(e),1);
                }
            }
            //r.GetMegaTile(new WorldPoint(new Point(0, 0, 0))).SetValue(new WorldPoint(0, 1, 1),1);
            //r.GetMegaTile(new WorldPoint(new Point(0, 0, 0))).SetValue(new WorldPoint(0, 2, 2),1);
            return r; 
        }

        //--------------
        //accessors
        //--------------
        public WorldPoint GetContainingMegaCornerTileWorldPoint(WorldPoint aPoint)
        {
            Func<int,int,int> roundDownNeg = delegate(int a, int b)
            {
                if (a >= 0)
                    return (a / b) * b;
                else
                    return ((a - b + 1) / b )* b;
            };
            var mod = new Point(roundDownNeg(aPoint.point.x, Grid.TilingSize.x),
                                roundDownNeg(aPoint.point.y, Grid.TilingSize.y),
                                roundDownNeg(aPoint.point.z, Grid.TilingSize.z));
            return new WorldPoint(mod);
        }

        public MegaTile GetMegaTile(WorldPoint aPoint)
        {
            aPoint = GetContainingMegaCornerTileWorldPoint(aPoint);
            MegaTile r = null;
            Grid.Data.TryGetValue(aPoint, out r);
            return r;
        }

        public int GetValue(WorldPoint aPoint)
        {
            return GetMegaTile(aPoint).GetValue(aPoint);
        }
        
        public int[,,] GetValuesInBox(Box aBox)
        {
            int[,,] r = new int[aBox.size.x, aBox.size.y, aBox.size.z];
            for(int i = 0; i < r.GetLength(0); i++){
                for(int j = 0; j < r.GetLength(1); j++){
                    for(int k = 0; k < r.GetLength(2); k++){
                        //TODO should cache megatiles
                        var point = new WorldPoint(new Point(i+aBox.corner.x,j+aBox.corner.y,k+aBox.corner.z));
                        r[i,j,k] = GetValue(point);
            }}}

            return r;
        }

        public List<MegaTile> GetMegaTilesIntersectingBox(Box aBox)
        {
            //TODO this function can possibly return an extra set of MTs to the right up front of aBox, fix it
            List<MegaTile> r = new List<MegaTile>();
            for (int i = aBox.LDBCorner.x; i < aBox.RUFCorner.x+Grid.TilingSize.x; i += Grid.TilingSize.x)
                for (int j = aBox.LDBCorner.y; j < aBox.RUFCorner.y+Grid.TilingSize.y; j += Grid.TilingSize.y)
                    for (int k = aBox.LDBCorner.z; k < aBox.RUFCorner.z+Grid.TilingSize.z; k += Grid.TilingSize.z)
                        r.Add(GetMegaTile(GetContainingMegaCornerTileWorldPoint(new WorldPoint(new Point(i, j, k)))));
            return r;
        }

        //--------------
        //setters
        //--------------
        public void SetValue(WorldPoint aPoint, int aValue)
        {
            GetMegaTile(aPoint).SetValue(aPoint, aValue);
        }

       
        //--------------
        //intersection functions
        //--------------
        //TODO


        //--------------
        //
        //--------------
        void CreateMegaTile(WorldPoint aPoint)
        {
            var corner = GetContainingMegaCornerTileWorldPoint(aPoint);
            if (Grid.Data.ContainsKey(corner))
                throw new Exception("trying to create megatile where there already is one)");
            Grid.Data [corner] = new MegaTile(corner, Grid.TilingSize);
        }

        //TODO megatile constructors using network data
    }

    [Serializable]
    public class MegaTileGrid
    {


        public Dictionary<WorldPoint,MegaTile> Data {get; private set;}
        public Point TilingSize{ get; private set; }

        public MegaTileGrid(Point aTilingSize){
            TilingSize = aTilingSize;
            Data = new Dictionary<WorldPoint, MegaTile>();
        }


        public WorldPoint GetContainingMegaCornerTileWorldPoint(WorldPoint aPoint)
        {
            Func<int,int,int> roundDownNeg = delegate(int a, int b)
            {
                if (a >= 0)
                return (a / b) * b;
                else
                return ((a - b + 1) / b )* b;
            };
            var mod = new Point(roundDownNeg(aPoint.point.x, TilingSize.x),
                                roundDownNeg(aPoint.point.y, TilingSize.y),
                                roundDownNeg(aPoint.point.z, TilingSize.z));
            return new WorldPoint(mod);
        }
        
        public MegaTile GetMegaTile(WorldPoint aPoint)
        {
            aPoint = GetContainingMegaCornerTileWorldPoint(aPoint);
            MegaTile r = null;
            Data.TryGetValue(aPoint, out r);
            return r;
        }

        public List<MegaTile> GetMegaTilesIntersectingBox(Box aBox)
        {
            List<MegaTile> r = new List<MegaTile>();
            for (int i = aBox.LDBCorner.x; i < aBox.RUFCorner.x; i += TilingSize.x)
                for (int j = aBox.LDBCorner.y; j < aBox.RUFCorner.y; j += TilingSize.y)
                    for (int k = aBox.LDBCorner.z; k < aBox.RUFCorner.z; k += TilingSize.z)
                    {
                        var mt = GetMegaTile(GetContainingMegaCornerTileWorldPoint(new WorldPoint(new Point(i, j, k))));
                        if(mt != null)
                            r.Add(mt);
                    }
            return r;
        }

        public MegaTileGrid SubGrid(Box aBox)
        {
            //TODO
            //note, this function is more or less thes ame as the above
            MegaTileGrid r = new MegaTileGrid(TilingSize);

            return r;
        }
    }

    [Serializable]
    public class MegaTile
    {
        Dictionary<Point,int> mChunkData = new Dictionary<Point, int>();
        public WorldPoint Corner{ get; private set; }
        public Point Size{ get; private set; }
        public Box WorldBox
        {
            get
            {
                return new Box(Corner.point,Size);
            }
        }
        public MegaTile(WorldPoint aCorner, Point aSize)
        {
            Corner = aCorner;
            Size = aSize;
        }
        public int GetValue(WorldPoint aPoint)
        {
            if (new Box(Corner.point, Size).ContainsPoint(aPoint.point))
            {
                int r;
                if(mChunkData.TryGetValue(aPoint.point, out r))
                    return r;
                return 0;
            }
            throw new Exception("point outside of bounds exception");
        }

        public void SetValue(WorldPoint aPoint, int aValue)
        {
            if (new Box(Corner.point, Size).ContainsPoint(aPoint.point))
                mChunkData[aPoint.point] = aValue;
            else
                throw new Exception("point outside of bounds exception");
        }

        public int[,,] GetDataAsArray(Box aWorldBox)
        {
            var rBox = WorldBox.Intersect(aWorldBox);
            if (rBox == null)
                return null;
            else
            {
                var sz = rBox.size;
                var r = new int[sz.x, sz.y, sz.z]; 
                foreach (var e in mChunkData)
                {
                    var pt = e.Key-Corner.point;
                    if(rBox.ContainsPoint(pt))
                        r[pt.x,pt.y,pt.z] = e.Value;
                }
                return r;
            }

        }

        public int[,,] GetDataAsArray()
        {
            var sz = WorldBox.size;
            var r = new int[sz.x, sz.y, sz.z]; 
            foreach (var e in mChunkData)
            {
                var pt = e.Key-Corner.point;
                r[pt.x,pt.y,pt.z] = e.Value;
            }
            return r;
        }
    }
}
