﻿using UnityEngine;
using System.Collections.Generic;
using Dota;
using System;

namespace Dota{
    //NOTE, this class does nothing for onw as we assume renderchunk and megatile are the same
    //represents one block data (may or may not be aligned to a megatile, so assume it's not
    //TODO this should have references to all megatiles in its domain and neigboring ones
    //NO, this should still have an int array of all blocks for fast neighbor checking

    //for neighbor checknig purposes, Data is expanded one block in all directions
    public class RenderChunk
    {
        public Box WorldBox {get; private set;}
        int[,,] Data { get; set; }

        //in world space, can go one point outside of WorldBox bounds due to extra data that is stored
        public int this[Point index]{
            get{
                var newIndex = index - WorldBox.corner;
                return Data[newIndex.x+1,newIndex.y+1,newIndex.z+1];
            }
            set{
                var newIndex = index - WorldBox.corner;
                Data[newIndex.x+1,newIndex.y+1,newIndex.z+1] = value;
            }
        }

        public RenderChunk(Box aWorldBox, MegaTileGrid aGrid)
        {
            WorldBox = aWorldBox;
            var sz = WorldBox.size;

            var expandedBox = new Box(WorldBox.corner - new Point(1, 1, 1), WorldBox.size + new Point(2, 2, 2));
            Data = new int[sz.x+2, sz.y+2, sz.z+2]; 

            var mts = aGrid.GetMegaTilesIntersectingBox(expandedBox);
            //Debug.Log(mts.Count);
            foreach (var e in mts)
            {
                var box = e.WorldBox.Intersect(expandedBox);
                //Debug.Log(e.WorldBox.ToString() + " " + expandedBox.ToString());
                //Debug.Log(box.ToString());
                foreach(var f in box.Range())
                {

                    var pt = f-expandedBox.corner;
                    var val = e.GetValue(new WorldPoint(f));

                    //if(val != 0)
                        //Debug.Log(pt);

                    Data[pt.x,pt.y,pt.z] = val;
                }
            }
        }

        //TODO DELETE
        public RenderChunk(MegaTile aTile)
        {
            WorldBox = aTile.WorldBox;
            //Corner = aTile.Corner;
            //Size = aTile.Size;
            Data = aTile.GetDataAsArray();
        }
    }
    
}