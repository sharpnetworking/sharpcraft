﻿


using System;
using System.Collections.Generic;
using System.Linq;
namespace Dota
{

    public enum CubeDirection
    {
        UP,DOWN,FRONT,BACK,LEFT,RIGHT
    }

    [Serializable]
    public struct Point
    {
        public static readonly Point zero = new Point(0,0,0);
        public static readonly Point one = new Point(1,1,1);
        public int Max{ get { return x > y ? (x > z ? x : z) : (y > z ? y : z); } }
        public int x, y, z;
        public Point(int aX, int aY, int aZ)
        {
            x = aX;
            y = aY;
            z = aZ;
        }
        public int this[int index]{
            get{
                if(index ==0) return x; else if(index == 1) return y; else if(index == 2) return z; else throw new IndexOutOfRangeException();
            }
            set{
                if(index ==0) x = value; else if(index == 1) y = value; else if(index == 2) z = value; else throw new IndexOutOfRangeException();
            }
        }
        public override string ToString(){return "(" + x + ", " + y + ", " + z + ")";}

        public static Point Parse(string aString)
        {
            var vals = aString.Replace("(", "").Replace(")", "").Split(',');
            if (vals.Length == 3)
            {
                Point r = new Point();
                r.x = Convert.ToInt32(vals[0]);
                r.y = Convert.ToInt32(vals[1]);
                r.z = Convert.ToInt32(vals[2]);
                return r;
            } else
                throw new Exception("Bad point format exception");
        }

        public Point ComponentWiseMultiply(Point p){return new Point(p.x*x,p.y*y,p.z*z);}
        public static Point operator +(Point p1, Point p2) { return new Point(p1.x + p2.x, p1.y + p2.y, p1.z + p2.z); }
        public static Point operator -(Point p1) { return new Point(-p1.x, -p1.y, -p1.z); }
        public static Point operator -(Point p1, Point p2) { return p1 + -p2; }
        public static Point operator *(Point p, int s){return new Point(p.x * s, p.y * s, p.z * s);}
        public static bool operator ==(Point o1, Point o2) { return Object.Equals(o1, o2); }
        public static bool operator !=(Point o1, Point o2) { return !(o1 == o2); }
        public override bool Equals(object comparand) { return this.ToString().Equals(comparand.ToString()); }
        public override int GetHashCode ()
        {
            int prime = 31;
            int result = 1;
            result = prime * result + x;
            result = prime * result + y;
            result = prime * result + z;
            return result;
        }

        public static Point[] StraightNeighbors(Point center)
        {
            return new Point[]{
                center + new Point(1,0,0),center + new Point(-1,0,0),
                center + new Point(0,1,0),center + new Point(0,-1,0),
                center + new Point(0,0,1),center + new Point(0,0,-1)
            };
        }
        

        public static IEnumerable<Point> Range(Point size)
        {
            Point p = new Point();
            for(p.z = 0; p.z < size.z; ++p.z)
                for (p.y = 0; p.y < size.y; ++p.y)
                    for (p.x = 0; p.x < size.x; ++p.x)
                        yield return p;
        }
        public static IEnumerable<Point> SymmetricRange(Point size)
        {
            Point p = new Point();
            for (p.z = -size.z; p.z <= size.z; ++p.z)
                for (p.y = -size.y; p.y <= size.y; ++p.y)
                    for (p.x = -size.x; p.x <= size.x; ++p.x)
                        yield return p;
        }
    }

    [Serializable]
    public struct Box{
        public Point LDBCorner{ get { return corner; } }
        public Point RUFCorner{ get { return corner+size; } }
        public Point corner,size;
        public Box(Point aCorner, Point aSize)
        {
            corner = aCorner;
            size = aSize;
        }
        public Point[] Corners()
        {
            Box temp = this;
            return Point.Range(Point.one * 2).Select(e => temp.corner + e.ComponentWiseMultiply(temp.size)).ToArray();    
        }

        public Box Intersect(Box aBox)
        {

            Func<int,int,int> min = (a,b) => a < b ? a : b;
            //TODO THIS FUNCTION IS BROKEN
            if (!(ContainsPoint(aBox.corner) || aBox.ContainsPoint(corner)))
                return new Box(Point.zero,Point.zero);
            else 
            {
                var first = this;
                Box r = new Box();
                Action<int> fnc = delegate(int i)
                {
                    if(first.corner[i] <= aBox.corner[i])
                    {
                        r.corner[i] = aBox.corner[i];
                        r.size[i] = min(first.corner[i] + first.size[i] - aBox.corner[i],aBox.size[i]);
                    } else
                    {
                        r.corner[i] = first.corner[i];
                        r.size[i] = min(aBox.corner[i] + aBox.size[i] - first.corner[i],first.size[i]);
                    }
                };
                fnc(0);
                fnc(1);
                fnc(2);
                //UnityEngine.Debug.Log(r.ToString());
                //return r; this does not work :O
                return new Box(r.corner,r.size);
            }
        }

        public bool ContainsBox(Box aBox)
        {
            return ContainsPoint(aBox.LDBCorner) && ContainsPoint(aBox.RUFCorner);
        }

        public bool ContainsPoint(Point aPosition)
        {
            return 
                (aPosition.x >= corner.x) &&
                (aPosition.y >= corner.y) &&
                (aPosition.z >= corner.z) &&
                (aPosition.x < corner.x + size.x) &&
                (aPosition.y < corner.y + size.y) &&
                (aPosition.z < corner.z + size.z);
        }

        public override string ToString(){return corner.ToString() + " " + size.ToString();}
        public static bool operator ==(Box o1, Box o2) { return Object.Equals(o1, o2); }
        public static bool operator !=(Box o1, Box o2) { return !(o1 == o2); }
        public override bool Equals(object comparand) { return this.ToString().Equals(comparand.ToString()); }

        public IEnumerable<Point> Range()
        {
            Point p = new Point();
            for(p.z = 0; p.z < size.z; ++p.z)
                for (p.y = 0; p.y < size.y; ++p.y)
                    for (p.x = 0; p.x < size.x; ++p.x)
                        yield return p + corner;
        }
    }

    [Serializable]
    public struct WorldPoint
    {
        public Point point;
        //dimensions!!!

        public WorldPoint(Point aPoint){point = aPoint;}
        public WorldPoint(int x, int y, int z) : this(new Point(x,y,z)){}

        public override string ToString(){return "World: " + point.ToString();}
        public static bool operator ==(WorldPoint o1, WorldPoint o2) { return Object.Equals(o1.point, o2.point); }
        public static bool operator !=(WorldPoint o1, WorldPoint o2) { return !(o1.point == o2.point); }
        public override bool Equals(object comparand) { return this.ToString().Equals(comparand.ToString()); }
        public override int GetHashCode (){
            return point.GetHashCode();
        }
    }

}