﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Dota
{
    public abstract class OperationBase
    {
    }

    public class OpSetBlockValue : OperationBase
    {
        class BlockChangeSet
        {
            public WorldPoint blockPos;
            public int oldVal;
            public int newVal;
        }

        BlockChangeSet[] mChangeSet;

        public OpSetBlockValue(WorldPoint[] aPos, int[] aNew)
        {
            mChangeSet = new BlockChangeSet[aPos.Length];
            for (int i = 0; i < aPos.Length; i++)
            {
                mChangeSet[i] = new BlockChangeSet();
                mChangeSet[i].blockPos = aPos[i];
                mChangeSet[i].newVal = aNew[i];
            }
        }

        public void Do(WorldData aData)
        {
            foreach(var e in mChangeSet)
            {
                e.oldVal = aData.GetValue(e.blockPos);
                aData.SetValue(e.blockPos,e.newVal);
            }
        }
    }
}