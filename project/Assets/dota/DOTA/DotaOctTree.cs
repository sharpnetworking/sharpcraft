﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Dota
{

    //oct tree that only takes points and splits only on powers of two
    public class PointPoTOctTree<T> where T : IEquatable<T>
    {
        public Point Offset{ get; set; }
        public bool HasOffset{ get { return Offset != Point.zero; } }
        public Node TopLevelNode{ get; private set; }
        public PointPoTOctTree(){}
        public PointPoTOctTree(uint aPowerOfTwoSize) //0 would be a single point
        {
            TopLevelNode = Node.CreateTopLevelNode(aPowerOfTwoSize);
        }

        //TODO TEST
        //this returns a shallow copy
        //this is slow as it will always reconstruct the topology
        public PointPoTOctTree<T> SubTree(Box aBox)
        {
            Box offBox = new Box(aBox.corner - Offset, aBox.size);
            PointPoTOctTree<T> r = new PointPoTOctTree<T>();
            r.Offset = Offset;
            r.TopLevelNode = TopLevelNode.SubTree(aBox);
            return r;
        }

        //public void Add(PointPoTOctTree<T> aTree){//TODO may need to expand height of tree}

        //-------
        //accessors/modifiers
        //-------
        public T GetValue(Point aPosition)
        {
            var ofPos = aPosition - Offset;
            if (!TopLevelNode.Bounds.ContainsPoint(ofPos))
            {
                UnityEngine.Debug.Log(ofPos);
                throw new System.Exception("Attepmting to read point outside of bounds");
            }
            return TopLevelNode.GetValue(ofPos);
        }
        public void SetValue(Point aPosition, T aVal)
        {
            var ofPos = aPosition - Offset;
            if (!TopLevelNode.Bounds.ContainsPoint(ofPos))
                throw new System.Exception("Attempting to add point outside of bounds, PointPoTOctTree does not support automatic expansion");
            TopLevelNode.SetValue(ofPos,aVal);
        }
        public IEnumerable<T> Range(Point corner, Point size)
        {
            foreach (var e in Point.Range(size))
                yield return GetValue(e + corner);
        }
        public IEnumerable<Pair> NonEmpty(Point corner, Point size)
        {
            foreach (var e in Point.Range(size))
            {
                var offE = e + corner;
                Pair r = new Pair(offE,GetValue(offE));
                if (!r.value.Equals(default(T)))
                    yield return r;
            }
        }
        public IEnumerable<Pair> NonEmpty()
        {
            return TopLevelNode.AllNonDefaultNonEmptyChildren().Select(e=>new Pair(e.position +Offset,e.value));
        }

        //--------
        //utility
        //--------
        static int LevelSize(uint aLevel)
        {
            return IntPow(2, aLevel);
        }
        static int IntPow(int x, uint pow)
        {
            int ret = 1;
            while ( pow != 0 )
            {
                if ( (pow & 1) == 1 )
                    ret *= x;
                x *= x;
                pow >>= 1;
            }
            return ret;
        }
        static uint NearestPowerOfTwo(uint x)
        {
            x--; // comment out to always take the next biggest power of two, even if x is already a power of two
            x |= (x >> 1);
            x |= (x >> 2);
            x |= (x >> 4);
            x |= (x >> 8);
            x |= (x >> 16);
            return (x+1);
        }


        //--------
        //embedded Pair class
        //--------
        public class Pair
        {
            public Point position;
            public T value;
            public Pair(Point aPoint, T aVal)
            {
                position = aPoint;
                value = aVal;
            }
        }

        //--------
        //embedded Node class
        //--------
        public class Node
        {
            public T Value{get; private set;}
            public Point Position{get; private set;} //bottom left back corner
            public Node Parent{ get; private set; }
            public Node[] Children{ get; private set; }
            public uint Level{ get; private set; }
            public uint Size{ get { return (uint)LevelSize(Level); } }
            public Box Bounds{ get { return new Box(Position, Point.one * (int)Size); } }


            public Node(Node aParent, uint aLevel, Point aPosition)
            {
                Level = aLevel;
                Parent = aParent;
                Position = aPosition;
                Children = null;
            }
            public static Node CreateTopLevelNode(uint aLevel)
            {
                return new Node(null,aLevel,Point.zero);
            }

            public bool IntersectsBox(Box aBox)
            {
                foreach (var e in aBox.Corners())
                    if (Bounds.ContainsPoint(e))
                        return true;
                foreach (var e in Bounds.Corners())
                    if (aBox.ContainsPoint(e))
                        return true;
                return false;
            }


            public int TreeIndex(Point aPosition)
            {
                int halfSize = (int)Size/2;
                int index = 0;
                if (aPosition.x >= Position.x + halfSize)
                    index |= (1 << 0);
                if (aPosition.y >= Position.y + halfSize)
                    index |= (1 << 1);
                if (aPosition.z >= Position.z + halfSize)
                    index |= (1 << 2);
                return (int)index;
            }
            public T GetValue(Point aPosition)
            {
                if (Level == 0)// && Position == aPosition)
                    return Value;
                var child = GetChildNode(TreeIndex(aPosition));
                if (child == null)
                    return default(T); //careful with value types
                return child.GetValue(aPosition);
            }
            public void SetValue(Point aPosition, T aVal)
            {
                if (Level == 0)// && Position == aPosition)
                    Value = aVal;
                else
                {
                    GetOrCreateChildNode(TreeIndex(aPosition)).SetValue(aPosition, aVal);
                }
            }

            public Node SubTree(Box aBox)
            {
                //NOTE this is not a deep copy...
                //the assumption being that this node DOES intersect aBox
                Node r = new Node(null, Level, Position);
                if (Level == 0)
                    r.Value = Value;
                else
                {
                    for (int i = 0; i < 8; i++)
                    {
                        var ch = GetChildNode(i);
                        if (ch != null && ch.IntersectsBox(aBox))
                            r.SetChildNode(i, ch.SubTree(aBox));
                    }
                }
                return r;
            }

            public IEnumerable<Pair> AllNonDefaultNonEmptyChildren()
            {
                if (Level == 0)
                {
                    if (!Value.Equals(default(T)))
                        yield return new Pair(Position,Value);
                } else if (Children != null)
                {
                    foreach(var f in Children.Where(e=>e!=null).Select(e=>e.AllNonDefaultNonEmptyChildren()).SelectMany(e=>e))
                        yield return f;
                }
            }


            public Node GetOrCreateChildNode(int aIndex)
            {
                if (Children == null)
                    initialize_children();
                if (Children [aIndex] == null)
                {
                    int halfSize = (int)Size/2;
                    byte index = (byte)aIndex;
                    Point nPos = new Point();
                    nPos.x = (index & (1<<0)) != 0 ? Position.x + halfSize : Position.x;
                    nPos.y = (index & (1<<1)) != 0 ? Position.y + halfSize : Position.y;
                    nPos.z = (index & (1<<2)) != 0 ? Position.z + halfSize : Position.z;
                    Children [aIndex] = new Node(this, Level - 1,nPos);
                }
                return Children [aIndex];
            }
            public Node GetChildNode(int aIndex)
            {
                if (Children == null)
                    return null;
                return Children[aIndex];
            }
            public void SetChildNode(int aIndex, Node aVal)
            {
                if (Children == null)
                    initialize_children();
                Children [aIndex] = aVal;
                aVal.Parent = this;
            }

            void initialize_children()
            {
                Children = new Node[8];
            }
        }
    }

}